# SPDX-FileCopyrightText: 2020 - 2024 CERN
# SPDX-FileCopyrightText: 2023 - 2024 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

[build-system]
requires = [
    'pip >= 24',
    'setuptools >= 62',
    'setuptools-scm[toml] ~= 8.0',
    'wheel',
]
build-backend = "setuptools.build_meta"

[project]
dynamic = ['version']
name = 'cernml-rltools'
authors = [
    {name = 'Nico Madysa', email = 'nico.madysa@cern.ch'},
]
description = 'Utilities for RL as part of the Geoff project'
readme = 'README.md'
license = {file='COPYING'}
dependencies = [
    'cernml-coi-utils[matplotlib] >= 0.3',
    'importlib-metadata; python_version<"3.10"',
    'typing-extensions',
    'numpy >= 1',
]
classifiers = [
    'Development Status :: 3 - Alpha',
    'Intended Audience :: Science/Research',
    'Natural Language :: English',
    'Operating System :: OS Independent',
    'Programming Language :: Python :: 3 :: Only',
    'Programming Language :: Python :: 3.9',
    'Programming Language :: Python :: 3.10',
    'Programming Language :: Python :: 3.11',
    'Programming Language :: Python :: 3.12',
    'Topic :: Scientific/Engineering :: Artificial Intelligence',
    'Topic :: Scientific/Engineering :: Physics',
    'Typing :: Typed',
]

[project.optional-dependencies]
test = [
    'click',
    'pytest',
    'pytest-cov',
]
doc = [
    'click',
    'types-docutils',
    'types-urllib3',
    'python-docs-theme',
    'sphinx',
]

[project.entry-points.'cernml.rltools.clicache']
tf_cache = 'cernml.rltools.clicache.tf_plugin:tf_plugin'
torch_cache = 'cernml.rltools.clicache.torch_plugin:torch_plugin'

[project.urls]
gitlab = 'https://gitlab.cern.ch/geoff/cernml-rltools/'

[tool.setuptools]
zip-safe = true
package-data = {'*' = ['py.typed']}

[tool.setuptools_scm]

[tool.black]
target-version = ['py39', 'py310', 'py311']

[tool.isort]
profile = 'black'
known_first_party = ['cernml']

[tool.ruff]
fix = true
src = ['src', 'test']
target-version = 'py39'

[tool.ruff.lint]
# Missing warnings:
# D* not raised in _*.py: https://github.com/astral-sh/ruff/issues/9561
extend-select = [
    'A', 'ARG', 'ASYNC', 'B', 'C4', 'C90', 'COM818', 'D', 'DTZ', 'EXE', 'FLY',
    'I', 'ICN', 'ISC', 'NPY', 'PD', 'PERF', 'PGH', 'PIE', 'PLR5501', 'PT',
    'PTH', 'PYI', 'RET', 'RSE', 'RUF', 'SIM', 'SLOT', 'T10', 'TD003', 'TRY',
    'UP',
]
ignore = [
    'D105',   # Missing docstring in magic method
    'D107',   # Missing docstring in __init__
    'D402',   # First line should not be the function's signature
    'D417',   # Missing argument description in the docstring
    'ISC001', # Implicit string concatenation, conflicts with autoformat
    'RUF012', # Mutable class attributes should use `typing.ClassVar`
              # TODO Waiting for Gymnasium#951 to get fixed.
    'SIM105', # Use contextlib.suppress(...) instead of try-except-pass
    'TRY003', # Avoid specifying long messages outside the exception class
    'UP037',  # Remove quotes from type annotation
]
allowed-confusables = [
    ' ',  # U+00A0 NO-BREAK SPACE
    '×',  # U+00D7 MULTIPLICATION SIGN
    '–',  # U+2013 EN DASH
    '−',  # U+2212 MINUS SIGN
]
flake8-pytest-style.fixture-parentheses = false
isort.split-on-trailing-comma = true
isort.known-first-party = ['cernml']
pycodestyle.max-doc-length = 72
pydocstyle.convention = 'google'
pydocstyle.ignore-decorators = ['overload', 'overrides']

[tool.ruff.lint.per-file-ignores]
'**/docs/*' = ['A001', 'ARG', 'D', 'INP']
'conftest.py' = ['ARG', 'D']
'tests/test_*.py' = ['A001', 'ARG', 'D', 'INP']
'examples/*.py' = ['ARG005', 'RET504']

[tool.pytest.ini_options]
addopts = "--doctest-modules"
asyncio_default_fixture_loop_scope = 'function'
testpaths = [
    'src',
    'tests',
]

[tool.pylint]
main.ignore-paths = ['docs']
main.ignore-patterns = ['.*\.pyi$', 'test_.*\.py$', 'conftest\.py$']
format.ignore-long-lines = '<?https?://\S+>?$'
'messages control'.disable = ['all']
'messages control'.enable = [
    'F',
    'bad-inline-option',
    'cell-var-from-loop',
    'missing-class-docstring',
    'missing-function-docstring',
    'missing-module-docstring',
    'singledispatch-method',
    'singledispatchmethod-function',
    'super-init-not-called',
    'typevar-double-variance',
    'typevar-name-incorrect-variance',
    'typevar-name-mismatch',
    'undefined-all-variable',
    'unrecognized-inline-option',
    'unrecognized-option',
]

[tool.coverage.run]
source = ['src']
branch = true

[tool.coverage.report]
exclude_also = [
    '^\s*@.*\.overload$',
    '^\s*def __del__',
    '^\s*except ImportError( as exc)?:$',
    '^\s*from .* import',
    '^\s*if .*\.TYPE_CHECKING:$',
    '^\s*if sys\.version_info ..? \(\d, \d+\):$',
    '^\s*import ',
    '^\s*raise NotImplementedError',
]

[tool.mypy]
disallow_untyped_defs = true
explicit_package_bases = true
mypy_path = '$MYPY_CONFIG_FILE_DIR/src'
exclude = ['^docs/']

[[tool.mypy.overrides]]
module = [
    'tensorflow.*',
    # Note: PyTorch _has_ type hints, but we want to ignore them regardless.
    # Our CI should not depend on PyTorch and so type hints won't be available.
    'torch.*',
]
ignore_missing_imports = true
