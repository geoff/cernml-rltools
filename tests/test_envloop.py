# SPDX-FileCopyrightText: 2020 - 2024 CERN
# SPDX-FileCopyrightText: 2023 - 2024 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

from __future__ import annotations

import typing as t

import gymnasium as gym
import numpy as np
import pytest

from cernml.rltools import envloop
from cernml.rltools.buffers import LimitedStepBuffer

if t.TYPE_CHECKING:
    from numpy.typing import NDArray


class MockEnv(gym.Env):
    metadata: dict[str, t.Any] = {"render.modes": []}

    def __init__(self, *, stop_at: int) -> None:
        self.observation_space = gym.spaces.Box(-1, 1, ())
        self.action_space = gym.spaces.Box(-1, 1, ())
        self.num_reset_calls = 0
        self.num_step_calls = 0
        self.episode_length = 0
        self._stop_at = stop_at

    def reset(
        self, *, seed: int | None = None, options: dict[str, t.Any] | None = None
    ) -> tuple[NDArray[np.double], dict[str, t.Any]]:
        super().reset(seed=seed, options=options)
        self.num_reset_calls += 1
        self.episode_length = 0
        return np.zeros(()), {}

    def step(
        self,
        action: NDArray[np.double],
    ) -> tuple[NDArray[np.double], t.SupportsFloat, bool, bool, dict[str, t.Any]]:
        self.num_step_calls += 1
        self.episode_length += 1
        terminated = self.episode_length >= self._stop_at
        truncated = False
        return np.zeros(()), np.zeros(()), terminated, truncated, {}

    def render(self, mode: str = "human") -> None:
        pass


class RandomAgent:
    # pylint: disable = too-few-public-methods, unused-argument
    def __init__(self, env: gym.Env):
        self.env = env

    def predict(
        self,
        obs: NDArray[np.double],
        state: None = None,
    ) -> tuple[NDArray[np.double], None]:
        return self.env.action_space.sample(), state


def test_mock_render() -> None:
    # Dummy test to improve coverage report.
    MockEnv(stop_at=1).render()


def test_bad_agent() -> None:
    with pytest.raises(TypeError):
        envloop.loop(None)  # type: ignore[arg-type]


def test_fallback_env() -> None:
    agent = RandomAgent(MockEnv(stop_at=1))
    envloop.loop(agent)


def test_no_env() -> None:
    with pytest.raises(ValueError, match="no environment passed"):
        envloop.loop(lambda obs: obs)


def test_record_steps_no_reset() -> None:
    env = envloop.RecordSteps(MockEnv(stop_at=1))
    action = env.action_space.sample()
    with pytest.raises(RuntimeError, match=r"^step\(\) called before reset\(\)$"):
        env.step(action)


def test_episodes() -> None:
    env = MockEnv(stop_at=3)
    agent = lambda _: env.action_space.sample()  # noqa: E731
    envloop.loop(agent, env, max_episodes=4)
    assert env.num_reset_calls == 4
    assert env.num_step_calls == 12
    assert env.episode_length == 3


def test_timesteps() -> None:
    env = MockEnv(stop_at=6)
    agent = lambda _: env.action_space.sample()  # noqa: E731
    envloop.loop(agent, env, max_timesteps=16)
    assert env.num_reset_calls == 3
    assert env.num_step_calls == 16
    assert env.episode_length == 4


def test_no_spurious_reset() -> None:
    env = MockEnv(stop_at=0)
    agent = lambda _: env.action_space.sample()  # noqa: E731
    envloop.loop(agent, env)
    assert env.num_reset_calls == 1
    assert env.num_step_calls == 1
    assert env.episode_length == 1


def test_noop() -> None:
    env = MockEnv(stop_at=0)
    agent = lambda _: env.action_space.sample()  # noqa: E731
    envloop.loop(agent, env, max_timesteps=0)
    assert env.num_reset_calls == 0
    assert env.num_step_calls == 0
    assert env.episode_length == 0


def test_collect_data() -> None:
    env = MockEnv(stop_at=10)
    agent = lambda _: env.action_space.sample()  # noqa: E731
    data = envloop.collect_data(agent, env, max_episodes=5)
    assert len(data) == 50
    assert env.num_reset_calls == 5
    assert env.num_step_calls == 50
    data = envloop.collect_data(
        agent,
        env,
        max_episodes=3,
        step_buffer=LimitedStepBuffer(maxlen=20),
    )
    assert len(data) == 20
    assert env.num_reset_calls == 8
    assert env.num_step_calls == 80


def test_random_data_args() -> None:
    env = MockEnv(stop_at=10)
    data = envloop.collect_random_data(env)
    assert len(data) == 1
    data = envloop.collect_random_data(env, 2)
    assert len(data) == 2
    data = envloop.collect_random_data(env, 3, max_episodes=None)  # type: ignore [call-overload]
    assert len(data) == 3
    data = envloop.collect_random_data(env, max_timesteps=4)
    assert len(data) == 4
    data = envloop.collect_random_data(env, max_episodes=5)
    assert len(data) == 5
    data = envloop.collect_random_data(env, max_episodes=5, max_episode_length=2)
    assert len(data) == 10
    with pytest.raises(TypeError):
        envloop.collect_random_data(env, 10, 20)  # type: ignore [call-overload]
    with pytest.raises(TypeError):
        envloop.collect_random_data(env, 10, max_episode_length=2)  # type: ignore [call-overload]
    with pytest.raises(TypeError):
        envloop.collect_random_data(env, 10, max_episodes=1)  # type: ignore [call-overload]
    with pytest.raises(TypeError):
        envloop.collect_random_data(env, 10, max_timesteps=1)  # type: ignore [call-overload]


def test_random_data_truncated() -> None:
    data = envloop.collect_random_data(MockEnv(stop_at=2), 20)
    assert len(data) == 20
    assert not any(data.get_terminated())
    assert all(data.get_truncated())


def test_random_data_terminated_and_truncated() -> None:
    data = envloop.collect_random_data(MockEnv(stop_at=1), 20)
    assert len(data) == 20
    assert all(data.get_terminated())
    assert all(data.get_truncated())


def test_random_data_terminated() -> None:
    data = envloop.collect_random_data(
        MockEnv(stop_at=1), max_timesteps=20, max_episode_length=2
    )
    assert len(data) == 20
    assert all(data.get_terminated())
    assert not any(data.get_truncated())


def test_random_data_max_episode_length() -> None:
    data = envloop.collect_random_data(
        MockEnv(stop_at=3),
        max_episodes=20,
        max_episode_length=2,
    )
    assert len(data) == 40
    envloop.collect_random_data(
        MockEnv(stop_at=1),
        max_episodes=20,
        max_episode_length=2,
        step_buffer=data,
    )
    assert len(data) == 60
