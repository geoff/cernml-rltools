# SPDX-FileCopyrightText: 2020 - 2024 CERN
# SPDX-FileCopyrightText: 2023 - 2024 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

from __future__ import annotations

import typing as t

import gymnasium as gym
import numpy as np
import pytest
from gymnasium.wrappers import TimeLimit

from cernml.rltools.buffers import LimitedStepBuffer, RecordSteps, Step, StepBuffer

if t.TYPE_CHECKING:
    from numpy.typing import NDArray


class SimpleEnv(gym.Env):
    observation_space = gym.spaces.Box(-1, 1, shape=(2,))
    action_space = gym.spaces.Box(-1, 1, shape=(2,))
    metadata: dict[str, t.Any] = {"render.modes": []}

    def __init__(self) -> None:
        self.pos = np.zeros(self.observation_space.shape)

    def reset(
        self, *, seed: int | None = None, options: dict[str, t.Any] | None = None
    ) -> tuple[NDArray[np.double], dict[str, t.Any]]:
        super().reset(seed=seed, options=options)
        self.pos = self.observation_space.sample()
        return self.pos.copy(), {}

    def step(
        self, action: NDArray[np.double]
    ) -> tuple[NDArray[np.double], t.SupportsFloat, bool, bool, dict[str, t.Any]]:
        self.pos += action
        return self.pos.copy(), -sum(self.pos**2), False, False, {}

    def render(self) -> t.Any:  # pragma: no cover
        super().render()


def dummy_step(terminated: bool = False) -> Step:
    return Step(np.zeros(()), np.zeros(()), 0.0, np.zeros(()), terminated, False)


@pytest.fixture
def step_buffer() -> StepBuffer:
    env = RecordSteps(TimeLimit(SimpleEnv(), 4))
    for _ in range(10):
        env.reset()
        done = False
        while not done:
            _, _, terminated, truncated, _ = env.step(env.action_space.sample())
            done = terminated or truncated
    return env.step_buffer


@pytest.fixture
def limited_buffer() -> LimitedStepBuffer:
    buf = LimitedStepBuffer[np.double, np.double](maxlen=10)
    env = RecordSteps(TimeLimit(SimpleEnv(), 4), buf)
    for _ in range(10):
        env.reset()
        done = False
        while not done:
            _, _, terminated, truncated, _ = env.step(env.action_space.sample())
            done = terminated or truncated
    return buf


def test_stepbuffer_getitem(step_buffer: StepBuffer) -> None:
    assert isinstance(step_buffer[0], Step)
    assert list(step_buffer[1:3]) == [step_buffer[i] for i in range(1, 3)]
    assert list(step_buffer[[3, 4, 5]]) == [step_buffer[i] for i in [3, 4, 5]]


def test_stepbuffer_setitem(step_buffer: StepBuffer) -> None:
    step = dummy_step()
    assert len(step_buffer) == 40
    step_buffer[:30] = []
    assert len(step_buffer) == 10
    step_buffer[0] = dummy_step()
    step_buffer[list(range(1, 10))] = [step for i in range(9)]
    assert list(step_buffer) == [step for _ in range(10)]


def test_stepbuffer_delitem(step_buffer: StepBuffer) -> None:
    del step_buffer[[1, 3, 5]]
    del step_buffer[0]
    del step_buffer[30:-3]
    assert len(step_buffer) == 33


def test_stepbuffer_extend(step_buffer: StepBuffer) -> None:
    assert len(step_buffer) == 40
    step_buffer.extend(step_buffer)
    assert len(step_buffer) == 80
    step_buffer.extend(
        [
            (
                np.zeros((2,)),
                np.zeros((2,)),
                0.0,
                np.zeros((2,)),
                False,
                False,
            )
        ]
    )
    assert len(step_buffer) == 81


def test_stepbuffer_listlike(step_buffer: StepBuffer) -> None:
    assert list(reversed(step_buffer)) == list(reversed(list(step_buffer)))
    assert step_buffer[0] in step_buffer


def test_stepbuffer_exceptions(step_buffer: StepBuffer) -> None:
    a_step = step_buffer[0]
    not_a_step = (np.zeros((2,)), np.zeros((2,)), 0.0, np.zeros((2,)), False)
    # Ignore type-checking: Passing incompatible types is the point of
    # this test.
    with pytest.raises(TypeError):
        StepBuffer([not_a_step])  # type: ignore[arg-type]
    with pytest.raises(TypeError):
        step_buffer[()]
    with pytest.raises(TypeError):
        step_buffer[0] = not_a_step  # type: ignore[call-overload]
    with pytest.raises(TypeError):
        step_buffer[:] = [not_a_step]  # type: ignore[list-item]
    with pytest.raises(ValueError, match="assigning 3 values to 2 elements"):
        step_buffer[[0, 1]] = [a_step, a_step, a_step]


def test_limitbuffer_maxlen(limited_buffer: LimitedStepBuffer) -> None:
    assert limited_buffer.maxlen == len(limited_buffer) == 10
    copy = LimitedStepBuffer(limited_buffer)
    assert copy.maxlen == len(copy) == 10
    copy = LimitedStepBuffer(limited_buffer, 20)
    assert copy.maxlen == 20 != len(copy) == 10
    with pytest.raises(TypeError):
        LimitedStepBuffer()
    with pytest.raises(TypeError):
        LimitedStepBuffer([])
    with pytest.raises(ValueError, match="maxlen must be positive"):
        LimitedStepBuffer(maxlen=-3)


def test_limitbuffer_getitem(limited_buffer: LimitedStepBuffer) -> None:
    buf = limited_buffer
    assert isinstance(buf[0], Step)
    assert list(buf[[3, 4, 5]]) == [buf[i] for i in [3, 4, 5]]


def test_limitbuffer_setitem(limited_buffer: LimitedStepBuffer) -> None:
    step = dummy_step()
    assert len(limited_buffer) == 10
    limited_buffer[0] = dummy_step()
    limited_buffer[list(range(1, 10))] = (step for i in range(9))
    assert list(limited_buffer) == [step for _ in range(10)]
    with pytest.raises(TypeError):
        limited_buffer[:] = []


def test_limitbuffer_delitem(limited_buffer: LimitedStepBuffer) -> None:
    del limited_buffer[[1, 3, 5]]
    del limited_buffer[0]
    del limited_buffer[::2]
    assert len(limited_buffer) == 3


def test_limitbuffer_extend(limited_buffer: LimitedStepBuffer) -> None:
    step = dummy_step()
    assert len(limited_buffer) == 10
    assert any(limited_buffer.get_truncated())
    limited_buffer.extend(9 * [step])
    assert any(limited_buffer.get_truncated())
    limited_buffer.extend([step])
    assert not any(limited_buffer.get_terminated())
    step = dummy_step(terminated=True)
    limited_buffer.clear()
    limited_buffer.extend(7 * [step])
    assert all(limited_buffer.get_terminated())


def test_readonly() -> None:
    buf = StepBuffer[np.double, np.double]()
    # Create a step from mutable objects. This deliberately ignores type
    # annotations.
    obs = np.zeros(2)
    action = np.zeros(2)
    reward = np.zeros(())
    next_obs = np.zeros(2)
    terminated = np.zeros((), dtype=bool)
    truncated = np.zeros((), dtype=bool)
    buf.append(
        obs,
        action,
        t.cast(float, reward),
        next_obs,
        t.cast(bool, terminated),
        t.cast(bool, truncated),
    )
    step = buf[0]
    # Mutate the objects.
    for array in obs, action, reward, next_obs, terminated, truncated:
        array += True
    # Check that the step has not been changed.
    assert np.array_equal(step.obs, [0.0, 0.0])
    assert np.array_equal(step.action, [0.0, 0.0])
    assert step.reward == 0.0
    assert np.array_equal(step.next_obs, [0.0, 0.0])
    assert step.terminated is False
    assert step.truncated is False
