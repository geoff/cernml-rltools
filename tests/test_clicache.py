# SPDX-FileCopyrightText: 2020 - 2024 CERN
# SPDX-FileCopyrightText: 2023 - 2024 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

from __future__ import annotations

import importlib
import sys
import typing as t
from argparse import ArgumentParser
from pathlib import Path
from types import ModuleType
from unittest.mock import Mock

import click
import pytest

from cernml.rltools.clicache import (
    CacheHandler,
    TrainingCacher,
    TrainWhen,
    add_cache_control_group,
    click_plugin,
    load_entry_points,
    metadata,
    train_or_load_module,
)


@pytest.fixture
def sys_modules(monkeypatch: pytest.MonkeyPatch) -> dict[str, ModuleType]:
    modules = sys.modules.copy()
    monkeypatch.setattr(sys, "modules", modules)
    return modules


@pytest.fixture
def cache_plugin(monkeypatch: pytest.MonkeyPatch) -> t.Iterator[Mock]:
    def return_entry_points(group: str) -> list[metadata.EntryPoint]:
        res: list[metadata.EntryPoint] = []
        if group == "cernml.rltools.clicache":
            ep = Mock(
                ["name", "value", "group", "load", "dist"],
                name=f"{metadata.__name__}.EntryPoint",
            )
            ep.load.return_value = mock_cache_plugin
            res.append(ep)
        return res

    mock_cache_plugin = Mock(name="MockCacheHandler")
    mock_cache_plugin.return_value.mock_add_spec(CacheHandler)
    mock_entry_points = Mock(
        name=f"{metadata.__name__}.entry_points", side_effect=return_entry_points
    )
    monkeypatch.setattr(metadata, "entry_points", mock_entry_points)
    try:
        return mock_cache_plugin
    finally:
        load_entry_points.cache_clear()


@pytest.fixture
def parser() -> ArgumentParser:
    parser = ArgumentParser()
    add_cache_control_group(parser, "cache control")
    return parser


def test_cache_plugin_works(cache_plugin: Mock) -> None:
    module = Mock(name="module")
    handler = cache_plugin(module)
    assert isinstance(handler, CacheHandler)


def test_entry_points_work(cache_plugin: Mock) -> None:
    cacher = TrainingCacher()
    assert cacher.plugins == [cache_plugin]


def test_find_handler_works(cache_plugin: Mock) -> None:
    module = Mock(name="module")
    cacher = TrainingCacher()
    handler = cacher.find_handler(module)
    assert handler == cache_plugin.return_value


def test_with_no_args(cache_plugin: Mock, parser: ArgumentParser) -> None:
    # Given:
    train_module = Mock(name="train_module")
    module = Mock(name="module")
    handler = cache_plugin.return_value
    # When:
    args = parser.parse_args([])
    result = train_or_load_module(args, train_module, module)
    # Then:
    train_module.assert_called_once_with(module)
    handler.load.assert_not_called()
    handler.save.assert_not_called()
    assert result == train_module.return_value


def test_with_auto_cache(cache_plugin: Mock, parser: ArgumentParser) -> None:
    # Given:
    train_module = Mock(name="train_module")
    module = Mock(name="module")
    path = Path("./cachedir")
    handler = cache_plugin.return_value
    # When:
    args = parser.parse_args([f"--cache={path}"])
    result = train_or_load_module(args, train_module, module)
    # Then:
    train_module.assert_not_called()
    cache_plugin.assert_called_once_with(module)
    handler.load.assert_called_once_with(path)
    handler.save.assert_not_called()
    assert result is None


def test_with_always_train(cache_plugin: Mock, parser: ArgumentParser) -> None:
    # Given:
    train_module = Mock(name="train_module")
    module = Mock(name="module")
    path = Path("./cachedir")
    handler = cache_plugin.return_value
    # When:
    args = parser.parse_args([f"--cache={path}", "--train=always"])
    result = train_or_load_module(args, train_module, module)
    # Then:
    train_module.assert_called_once_with(module)
    cache_plugin.assert_called_once_with(module)
    handler.load.assert_not_called()
    handler.save.assert_called_once_with(path)
    assert result == train_module.return_value


def test_with_missing_cache(cache_plugin: Mock, parser: ArgumentParser) -> None:
    # Given:
    train_module = Mock(name="train_module")
    module = Mock(name="module")
    path = Path("./cachedir")
    handler = cache_plugin.return_value
    handler.load.side_effect = OSError()
    # When:
    args = parser.parse_args([f"--cache={path}"])
    result = train_or_load_module(args, train_module, module)
    # Then:
    train_module.assert_called_once_with(module)
    cache_plugin.assert_called_once_with(module)
    handler.load.assert_called_once_with(path)
    handler.save.assert_called_once_with(path)
    assert result == train_module.return_value


def test_tf_plugin_no_eager_import(sys_modules: dict[str, ModuleType]) -> None:
    # Given:
    sys_modules.clear()
    sys_modules["sys"] = sys
    # When:
    importlib.import_module("cernml.rltools.clicache.tf_plugin")
    # Then:
    assert "tensorflow" not in sys_modules
    assert "cernml.rltools.clicache.tf_plugin" in sys_modules


def test_torch_plugin_no_eager_import(sys_modules: dict[str, ModuleType]) -> None:
    # Given:
    sys_modules.clear()
    sys_modules["sys"] = sys
    # When:
    importlib.import_module("cernml.rltools.clicache.torch_plugin")
    # Then:
    assert "torch" not in sys_modules
    assert "cernml.rltools.clicache.torch_plugin" in sys_modules


class TestClickPlugin:
    def make_command(
        self,
        expected_cache: Path | None = None,
        expected_train: TrainWhen | str = "auto",
    ) -> click.Command:
        expected_train = TrainWhen(expected_train or "auto")

        @click.command()
        @click_plugin.cache_control_options()
        def click_command(cache: Path | None, train: TrainWhen) -> None:
            assert cache == expected_cache
            assert train == expected_train

        return click_command

    def test_params(self) -> None:
        command = self.make_command()
        assert len(command.params) == 2
        assert command.params[0].name == "cache"
        assert command.params[0].type.name == "cache path"
        assert command.params[1].name == "train"
        assert command.params[1].type.name == "train mode"

    def test_defaults(self) -> None:
        command = self.make_command()
        command.main([], standalone_mode=False)

    def test_custom_train(self) -> None:
        command = self.make_command(expected_train="never")
        command(["--train", "never"], standalone_mode=False)

    def test_custom_cache(self) -> None:
        expected_cache = Path("test/cache/path")
        command = self.make_command(expected_cache=expected_cache)
        command(["--cache", str(expected_cache)], standalone_mode=False)

    def test_bad_train(self) -> None:
        command = self.make_command()
        with pytest.raises(click.BadParameter, match="^'nev' is not one of"):
            command(["--train", "nev"], standalone_mode=False)
