# SPDX-FileCopyrightText: 2020 - 2024 CERN
# SPDX-FileCopyrightText: 2023 - 2024 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

from __future__ import annotations

import enum
import importlib
import sys
import typing as t
from unittest.mock import MagicMock, patch

import pytest


class _TestEnum(enum.Enum):
    """We only use this enum to lie to the type checker."""

    FIRST = "first"
    SECOND = "second"
    THIRD = "third"


def get_str_enum() -> type[enum.StrEnum]:
    # Prevent `enum.StrEnum` from being found, even if it exists.
    mock_enum = MagicMock(Enum=enum.Enum, spec=True)
    with patch.dict(sys.modules, {"enum": mock_enum}):
        # Force `cernml.rltools._strenum` to be reloaded.
        package_name = "cernml.rltools._strenum"
        sys.modules.pop(package_name, None)
        str_enum = importlib.import_module(package_name)
    return str_enum.StrEnum


StrEnum = get_str_enum()


def find_all_str_enums() -> tuple[type[enum.StrEnum], ...]:
    res = [StrEnum]
    found = getattr(enum, "StrEnum", None)
    if found:
        res.append(found)
    return tuple(res)


ALL_STR_ENUMS = find_all_str_enums()


def test_import_hack() -> None:
    assert StrEnum.__module__ == "cernml.rltools._strenum"


@pytest.mark.parametrize("enum_class", ALL_STR_ENUMS)
def test_zero_params(enum_class: t.Callable[..., type[_TestEnum]]) -> None:
    the_enum = enum_class("TheEnum", ["FIRST", "SECOND", "THIRD"])
    assert repr(the_enum.FIRST) == "<TheEnum.FIRST: 'first'>"
    assert repr(the_enum.SECOND) == "<TheEnum.SECOND: 'second'>"
    assert repr(the_enum.THIRD) == "<TheEnum.THIRD: 'third'>"


@pytest.mark.parametrize("enum_class", ALL_STR_ENUMS)
def test_one_param(enum_class: t.Callable[..., type[_TestEnum]]) -> None:
    the_enum = enum_class("TheEnum", {"FIRST": "1", "SECOND": "2", "THIRD": "3"})
    assert repr(the_enum.FIRST) == "<TheEnum.FIRST: '1'>"
    assert repr(the_enum.SECOND) == "<TheEnum.SECOND: '2'>"
    assert repr(the_enum.THIRD) == "<TheEnum.THIRD: '3'>"


@pytest.mark.parametrize("enum_class", ALL_STR_ENUMS)
def test_two_params(enum_class: t.Callable[..., type[_TestEnum]]) -> None:
    the_enum = enum_class(
        "TheEnum",
        {
            "FIRST": (b"\xc3\xa4", "utf-8"),
            "SECOND": (b"\xc3\xb6", "utf-8"),
            "THIRD": (b"\xc3\xbc", "utf-8"),
        },
    )
    assert repr(the_enum.FIRST) == "<TheEnum.FIRST: 'ä'>"
    assert repr(the_enum.SECOND) == "<TheEnum.SECOND: 'ö'>"
    assert repr(the_enum.THIRD) == "<TheEnum.THIRD: 'ü'>"


@pytest.mark.parametrize("enum_class", ALL_STR_ENUMS)
def test_three_params(enum_class: t.Callable[..., type[_TestEnum]]) -> None:
    the_enum = enum_class(
        "TheEnum",
        {
            "FIRST": (b"\xc3\xa4", "ascii", "backslashreplace"),
            "SECOND": (b"\xc3\xb6", "ascii", "backslashreplace"),
            "THIRD": (b"\xc3\xbc", "ascii", "backslashreplace"),
        },
    )
    assert repr(the_enum.FIRST) == r"<TheEnum.FIRST: '\\xc3\\xa4'>"
    assert repr(the_enum.SECOND) == r"<TheEnum.SECOND: '\\xc3\\xb6'>"
    assert repr(the_enum.THIRD) == r"<TheEnum.THIRD: '\\xc3\\xbc'>"


@pytest.mark.parametrize("enum_class", ALL_STR_ENUMS)
def test_not_a_string(enum_class: t.Callable[..., type[_TestEnum]]) -> None:
    with pytest.raises(TypeError, match="is not a string$"):
        enum_class("TheEnum", {"NAME": 1})


@pytest.mark.parametrize("enum_class", ALL_STR_ENUMS)
def test_not_a_codec(enum_class: t.Callable[..., type[_TestEnum]]) -> None:
    with pytest.raises(TypeError, match="^encoding must be a string"):
        enum_class("TheEnum", {"NAME": (b"", 1)})


@pytest.mark.parametrize("enum_class", ALL_STR_ENUMS)
def test_not_an_error_name(enum_class: t.Callable[..., type[_TestEnum]]) -> None:
    with pytest.raises(TypeError, match="^errors must be a string"):
        enum_class("TheEnum", {"NAME": (b"\xc3\xa4", "ascii", 1)})


@pytest.mark.parametrize("enum_class", ALL_STR_ENUMS)
def test_too_many_args(enum_class: t.Callable[..., type[_TestEnum]]) -> None:
    with pytest.raises(TypeError, match="^too many arguments for str"):
        enum_class("TheEnum", {"NAME": ("", "", "", "")})
