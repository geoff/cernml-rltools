# SPDX-FileCopyrightText: 2020 - 2024 CERN
# SPDX-FileCopyrightText: 2023 - 2024 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

from __future__ import annotations

import typing as t

import gymnasium as gym
import numpy as np

from cernml.rltools import wrappers

if t.TYPE_CHECKING:
    from numpy.typing import NDArray


class SimpleEnv(gym.Env):
    observation_space = gym.spaces.Box(-1, 1, shape=(2,))
    action_space = gym.spaces.Box(-1, 1, shape=(2,))
    reward_range = (-1.0, 0.0)
    metadata: dict[str, t.Any] = {"render.modes": []}

    def __init__(self, render_mode: str | None = None) -> None:
        self.render_mode = render_mode
        self.pos = np.zeros(self.observation_space.shape)

    def reset(
        self, *, seed: int | None = None, options: dict[str, t.Any] | None = None
    ) -> tuple[NDArray[np.double], dict[str, t.Any]]:
        super().reset(seed=seed, options=options)
        self.pos = self.observation_space.sample()
        return self.pos.copy(), {}

    def step(
        self, action: NDArray[np.double]
    ) -> tuple[NDArray[np.double], t.SupportsFloat, bool, bool, dict[str, t.Any]]:
        self.pos += action
        return self.pos.copy(), -sum(self.pos**2), False, False, {}

    def render(self) -> None:
        return None


def test_render() -> None:
    # Dummy test to improve coverage report.
    SimpleEnv().render()


def test_end_on_bad_reward() -> None:
    env = wrappers.EndOnOutOfRangeReward(SimpleEnv())
    assert env.reward_range == env.unwrapped.reward_range
    env.reset()
    t.cast(SimpleEnv, env.unwrapped).pos *= 0.0
    assert env.observation_space.shape is not None
    step = 0.5 * np.ones(env.observation_space.shape)
    _, reward, terminated, truncated, info = env.step(step)
    assert np.isclose(float(reward), -0.5)
    assert not terminated
    assert not truncated
    assert info.get("reward_limits") is None  # back-compat check
    assert not info.get("EndOnOutOfRangeReward.truncated")
    _, reward, terminated, truncated, info = env.step(step)
    assert np.isclose(float(reward), -2.0)
    assert not terminated
    assert truncated
    assert info.get("reward_limits") is None  # back-compat check


def test_end_on_good_reward() -> None:
    env = wrappers.EndOnOutOfRangeReward(SimpleEnv(), upper=-0.1)
    assert env.reward_range == (env.unwrapped.reward_range[0], -0.1)
    env.reset()
    t.cast(SimpleEnv, env.unwrapped).pos[:] = [1.0, 0.0]
    step = np.array([-0.5, 0.0])
    _, reward, terminated, truncated, info = env.step(step)
    assert np.isclose(float(reward), -0.25)
    assert not terminated
    assert not truncated
    assert info.get("reward_limits") is None  # back-compat check
    _, reward, terminated, truncated, info = env.step(step)
    assert np.isclose(float(reward), 0.0)
    assert terminated
    assert not truncated
    assert info.get("reward_limits") is None  # back-compat check
