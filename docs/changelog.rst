..
    SPDX-FileCopyrightText: 2020-2024 CERN
    SPDX-FileCopyrightText: 2023-2024 GSI Helmholtzzentrum für Schwerionenforschung
    SPDX-FileNotice: All rights not expressly granted are reserved.

    SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

:tocdepth: 3

Changelog
=========

.. currentmodule:: cernml.rltools

.. _semantic-versioning:

This package uses a variant of `Semantic Versioning <https://semver.org/>`_
that makes additional promises during the initial development (major version
0): whenever breaking changes to the public API are published, the first
non-zero version number will increase. This means that code that uses version
0.x.y of this package will continue to work with version 0.x.y+1, but may break
with version 0.x+1.0.

Unreleased
----------

No changes yet!

v0.12
-----

v0.12.0
^^^^^^^

Breaking changes
~~~~~~~~~~~~~~~~
- Update to :doc:`cernml-coi <coi:index>` v0.9, from `Gym
  <https://github.com/openai/gym/>`_ to :doc:`Gymnasium <gym:index>`.
- Drop support for Python 3.7.
- Remove deprecated ``wrappers.RewardGoal``.
- Replace *disabled* argument of `.RenderOnStep` with
  *enabled*, which may be an enum `.EnabledMode`.
- Rename arguments of `.LogRewards`.
- The unit tests have been moved out of the package proper. Apologies to
  everyone who relied on ``from cernml.rltools.test_envloop import MockEnv``.
- `.RecordSteps` now raises a `RuntimeError` instead of a `TypeError` if
  :func:`~gymnasium.Env.step()` is called before
  :func:`~gymnasium.Env.reset()`.
- The `clicache` package has been largely reworked:

  - The ``clicache.Cache`` class has been renamed to `clicache.CacheHandler`
    and been turned into a `Protocol`.

  - The arguments of `.TrainingCacher` have been switched around. Its
    constructor now takes arguments *cache* and *train*, whereas its
    `~.TrainingCacher.__call__()` overload takes the function *train_module*
    and the *module* to train and cache.

  - Cache handlers are no longer expected to raise `.SkipCache` if they're not
    responsible for a module. Instead, a dedicated `.CachePlugin` function
    should return None in such a case. (The exception-based method is still
    supported.)

  - Cache plugins are no longer registered via a class attribute
    ``default_caches`` on `.TrainingCacher`. Instead, they are registered via
    the entry point :ep:`cernml.rltools.clicache`.

  - Adding additional plugins to a `.TrainingCacher` instance still works, but
    the attribute has been renamed to `~.TrainingCacher.plugins`.

  - The builtin plugins `.tf_plugin` and `.torch_plugin` now load their
    respective package as late as possible. This reduces the impact in
    environments where both packages are installed but only one is used.

    **This may break detection for some rare module classes!**

  - All exceptions have been renamed and now subclass `.CacheError`. The names
    have changed as follows:

    =================== ===================
          Old Name            New Name
    =================== ===================
    —                   `.CacheError`
    ``NoSuitableCache``	`.NoHandlerError`
    ``NoWeights``       `.NoCacheFileError`
    `.SkipCache`        `.SkipCache`
    =================== ===================

Additions
~~~~~~~~~
- `.RenderOnStep.EnabledMode`.
- Support in `.LogRewards` for *info* dict returned by `reset()
  <gymnasium.Env.reset>`.
- `clicache.TrainWhen` now treats strings case-insensitively. This affects both
  `.add_cache_control_group()` and `.TrainingCacher.__call__()`. Users can also
  observe this via the :option:`--train` CLI argument.

Bug fixes
~~~~~~~~~
- Fix outdated docstring of `.AtEndOfEpisode`.
- `.EndOnOutOfRangeReward` relied on deprecated attribute
  `gymnasium.Env.reward_range`.

Other changes
~~~~~~~~~~~~~
- Add documentation for this package.
- Make `buffers` classes generic over `~numpy.dtype`.
- Use `cernml.gym_utils.Scaler` in `.RescaleObservation`.
- Make type annotations of `envloop`, `renderers` and `wrappers` more detailed.
- Replace flake8, pylint, isort with `ruff <https://docs.astral.sh/ruff/>`_.

v0.11
-----

v0.11.1
^^^^^^^

Other changes
~~~~~~~~~~~~~
- Change license of this package to an open-source one.
- Change project URL.
- Switch project layout to src-layout_.
- Update linter and formatter setup. Start using `pre-commit
  <https://pre-commit.com/>`_.
- Switch to Acc-Py CI templates v2.

.. _src-layout:
   https://setuptools.pypa.io/en/latest/userguide/package_discovery.html
   #src-layout

v0.11.0
^^^^^^^

Breaking changes
~~~~~~~~~~~~~~~~
- Refactor `.RenderWrapper` to use `~cernml.mpl_utils.FigureRenderer`.
- Add dependency on :doc:`cernml-coi-utils <utils:index>`.

Additions
~~~~~~~~~
- Add module `renderers`.

Other changes
~~~~~~~~~~~~~
- da244d6 (tag: v0.11.0) Switch to f-strings to silence PyLint.

v0.10
-----

v0.10.6
^^^^^^^

Bug fixes
~~~~~~~~~
- Relax dependency version on `cernml.coi` to include v0.8.
- Fix outdated type annotations.

Other changes
~~~~~~~~~~~~~
- Don't run Black on auto-generated :file:`_version.py`.

v0.10.5
^^^^^^^

Bug fixes
~~~~~~~~~
- Relax dependency version on `cernml.coi` to include v0.7.

Other changes
~~~~~~~~~~~~~
- Use isort.

v0.10.4
^^^^^^^

Bug fixes
~~~~~~~~~
- Relax dependency version on `cernml.coi` to include v0.6.

v0.10.3
^^^^^^^

Bug fixes
~~~~~~~~~
- Relax dependency version on `cernml.coi` to include v0.5.

Other changes
~~~~~~~~~~~~~
- Make Mypy handle namespace packages correctly.
- Also build wheels for distribution.

v0.10.2
^^^^^^^

Additions
~~~~~~~~~
- Mark package as completely type annotated.

Bug fixes
~~~~~~~~~
- Remove stray call to
  :meth:`~matplotlib.backend_bases.FigureCanvasBase.draw_idle()` in
  :doc:`examples/index`.

Other changes
~~~~~~~~~~~~~
- Refactor logic of `.TrainingCacher` for readability.
- Refactor `clicache` module into a package.
- Replace :file:`setup.cfg` with :file:`pyproject.toml`.
- Switch versioning to `setuptools-scm <https://setuptools.readthedocs.io/>`_.
- Require `PyTest 6 <https://pytest.org>`_ so that its configs can be put into
  :file:`pyproject.toml`.
- Remove dependency on TensorFlow and PyTorch from unit tests.

v0.10.1
^^^^^^^

- Relax dependency version on `cernml.coi` to include v0.4.

v0.10.0
^^^^^^^

Breaking changes
~~~~~~~~~~~~~~~~
- Change arguments of `.collect_random_data()`; meaning of various limits is
  clarified.

Other changes
~~~~~~~~~~~~~
- Add usage example to docs of `.loop()`.

v0.9
----

v0.9.0
^^^^^^

Breaking changes
~~~~~~~~~~~~~~~~
- Rename package from ``cern_awake_mltools`` to `cernml.rltools`.
- Remove `~cernml.coi.SeparableEnv`, it is now provided by `cernml.coi`.
- Remove :ref:`deprecated <changelog:v0.8.4>` ``LogEpisodes``.

Bug fixes
~~~~~~~~~
- Remove superfluous attribute ``__version__`` from `clicache`.

Other changes
~~~~~~~~~~~~~
- Switch autoformatting from `YAPF <https://github.com/google/yapf>`_ to `Black
  <https://black.readthedocs.io/>`_.
- Remove :keyword:`__all__ <import>`. Decisions are hard.

v0.8
----

v0.8.9
^^^^^^

Deprecations
~~~~~~~~~~~~
- Mark entire package ``cernm_awake_mltools`` as deprecated. Prepare new
  release with new name.

v0.8.8
^^^^^^

Additions
~~~~~~~~~
- `.RescaleObservation`.

Other changes
~~~~~~~~~~~~~
- Improve test coverage of `buffers`, `envloop` and `wrappers`.

v0.8.7
^^^^^^

Bug fixes
~~~~~~~~~
- Applied `~gymnasium.wrappers.TimeLimit` and `.RecordSteps` in the wrong order
  in `.collect_random_data()`.

v0.8.6
^^^^^^

Additions
~~~~~~~~~
- `.collect_data()` and `.collect_random_data()`.

Bug fixes
~~~~~~~~~
- Accidental assumption that `.Agent` has an attribute ``env``.

Other changes
~~~~~~~~~~~~~
- Annotate types in `envloop.loop()`.
- Add :keyword:`__all__ <import>` to `envloop`.
- Reduce number of imports by replacing `collections.abc` interfaces with their
  aliases in `typing`.

v0.8.5
^^^^^^

Additions
~~~~~~~~~
- `.LogRewards.end_training()`.

Other changes
~~~~~~~~~~~~~
- Document `LogRewards.__init__() <.LogRewards>`.

v0.8.4
^^^^^^

Deprecations
~~~~~~~~~~~~
- ``LogEpisodes``; use `.LogRewards` instead.

Additions
~~~~~~~~~
- Wrappers `.EndOnOutOfRangeReward`, `.ExtractObservation`, `.DontRender` and
  `.LogRewards`.
- Example :doc:`examples/run_log_rewards`.

Bug fixes
~~~~~~~~~
- Remove outdated docs on `LogEpisodes <.LogRewards>`.

v0.8.3
^^^^^^

Additions
~~~~~~~~~
- `.StepBuffer.clear()`.

Bug fixes
~~~~~~~~~
- Add explicit colors to plots of `LogEpisodes <.LogRewards>`.

v0.8.2
^^^^^^

Bug fixes
~~~~~~~~~
- Bad marker handling in `LogEpisodes <.LogRewards>`.

v0.8.1
^^^^^^

Bug fixes
~~~~~~~~~
- Off-by-one error in `LogEpisodes <.LogRewards>`.

v0.8.0
^^^^^^

Breaking changes
~~~~~~~~~~~~~~~~
- `.AtEndOfEpisode` passes *info* to its callback.

Additions
~~~~~~~~~
- `.wrap_with_chain()`.
- `LogEpisodes <.LogRewards>` now also logs current/final reward and
  :term:`success` state.

Other changes
~~~~~~~~~~~~~
- Add type annotations to `wrappers`.
- Add :keyword:`__all__ <import>` to `wrappers`.

v0.7
----

v0.7.6
^^^^^^

Bug fixes
~~~~~~~~~
- `.RecordSteps.step()` fails on scalar-type *action*.


v0.7.5
^^^^^^

Other changes
~~~~~~~~~~~~~
- Fix broken doctest.

v0.7.4
^^^^^^

Bug fixes
~~~~~~~~~
- Missing copying in `.StepBuffer.append()`.

Other changes
~~~~~~~~~~~~~
- Add :keyword:`__all__ <import>` to `buffers`.

v0.7.3
^^^^^^

Additions
~~~~~~~~~
- Argument *disabled* (now *enabled*) to `.RenderOnStep`.

v0.7.2
^^^^^^

Bug fixes
~~~~~~~~~
- `.RenderOnStep` called `Figure.show() <matplotlib.figure.Figure.show>` more
  than once.

v0.7.1
^^^^^^

Bug fixes
~~~~~~~~~
- Typo in attribute access in `.RenderWrapper`.

v0.7.0
^^^^^^

Breaking changes
~~~~~~~~~~~~~~~~
- Change API of `.RenderWrapper`.

Bug fixes
~~~~~~~~~
- Improve handling of the Matplotlib GUI loop in module `wrappers`.

v0.6
----

v0.6.1
^^^^^^

Bug fixes
~~~~~~~~~
- Spurious reset in `envloop`.

v0.6.0
^^^^^^

Additions
~~~~~~~~~
- `sepenv.SeparableEnv <cernml.coi.SeparableEnv>`.

Bug fixes
~~~~~~~~~
- Colors of plot in in `LogEpisodes <.LogRewards>`.

v0.5
----

v0.5.2
^^^^^^

Additions
~~~~~~~~~
- `.LimitedStepBuffer`.
- `.StepBuffer` derives from `collections.abc.Sequence`.
- Allow deleting items from `.StepBuffer`.
- Allow indexing `.StepBuffer` by lists of integers.

Bug fixes
~~~~~~~~~
- NumPy integers in `.StepBuffer` slicing.
- Accidentally returning subclasses from `.StepBuffer` slicing.

Other changes
~~~~~~~~~~~~~
- fa32b4f Add unit tests.

v0.5.1
^^^^^^

Other changes
~~~~~~~~~~~~~
- Add Continuous Integration.

v0.5.0
^^^^^^

Bug fixes
~~~~~~~~~
- `LogEpisodes <.LogRewards>` didn't handle handle empty log and unfinished
  episodes.
- Canvas updating in `.RenderWrapper`.
- `.RenderOnStep` didn't render on :func:`~gymnasium.Env.reset()`.

v0.4
----

v0.4.0
^^^^^^

Breaking changes
~~~~~~~~~~~~~~~~
- Restrict dependencies, use compatible-release_ operator.

.. _compatible-release:
   https://packaging.python.org/en/latest/specifications/version-specifiers/
   #compatible-release

Additions
~~~~~~~~~
- `.StepBuffer` support for :func:`reversed()`, :keyword:`in` and
  `~.StepBuffer.copy()` for compatibility with :ref:`mutable sequence types
  <std:typesseq-mutable>`.

Bug fixes
~~~~~~~~~
- Only wrap callables in `envloop` if they have no `~.Agent.predict()` method.

Other changes
~~~~~~~~~~~~~
- Extend docs on `.StepBuffer`.
- Make :func:`repr()` output of `.StepBuffer` more concise.
- Skip superfluous consistency checks in `.StepBuffer`.
- Pre-bind `~.Agent.predict()` in `envloop`.

v0.3
----

v0.3.0
^^^^^^

Breaking changes
~~~~~~~~~~~~~~~~
- Switch project name (but not package name) from ``cern_awake_mltools`` to
  ``cern-awake-mltools``.

v0.2
----

v0.2.0
^^^^^^

Breaking changes
~~~~~~~~~~~~~~~~
- Rename package from ``awake_mltools`` to ``cern_awake_mltools`` to avoid name
  clashes.

v0.1
----

v0.1.3
^^^^^^

Other changes
~~~~~~~~~~~~~
- Add `PyTest <https://pytest.org/>`_ config file.
- Make clicache `std:doctest` compatible with Tensorflow 1.

v0.1.2
^^^^^^

Bug fixes
~~~~~~~~~
- Wrong function call in `.RenderOnStep`.

v0.1.1
^^^^^^

Additions
~~~~~~~~~
- Add README and project URLs.
- The modules `buffers`, `envloop` and `wrappers`.

v0.1.0
^^^^^^

The dawn of time.
