..
    SPDX-FileCopyrightText: 2020-2024 CERN
    SPDX-FileCopyrightText: 2023-2024 GSI Helmholtzzentrum für Schwerionenforschung
    SPDX-FileNotice: All rights not expressly granted are reserved.

    SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

CERNML RL Tools
===============

CERN ML is the project of bringing numerical optimization, machine learning and
reinforcement learning to the operation of the CERN accelerator complex.

This is a collection of useful classes and functions for reinforcement learning
that have come up repeatedly while using the COI_.

A strong effort is made to keep these helpers neutral w.r.t. the ML framework
that one uses (TensorFlow, PyTorch, Theano, etc.). Merge requests are welcome
whenever it is likely that a class or function is useful to multiple projects
and there is a benefit to having one central implementation.

This repository can be found online on CERN's Gitlab_.

.. _COI: https://gitlab.cern.ch/geoff/cernml-coi/
.. _Gitlab: https://gitlab.cern.ch/geoff/cernml-rltools/

.. toctree::
    :maxdepth: 2

    examples/index
    api/index
    changelog
