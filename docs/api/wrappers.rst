..
    SPDX-FileCopyrightText: 2020-2024 CERN
    SPDX-FileCopyrightText: 2023-2024 GSI Helmholtzzentrum für Schwerionenforschung
    SPDX-FileNotice: All rights not expressly granted are reserved.

    SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

:tocdepth: 2

Environment Wrappers for RL
===========================

.. automodule:: cernml.rltools.wrappers
    :exclude-members: action, close, observation, render, reset, step

.. data:: DType
    :type: typing.TypeVar

    The generic type variable for custom `~numpy.dtype` overrides.
