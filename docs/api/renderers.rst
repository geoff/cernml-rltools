..
    SPDX-FileCopyrightText: 2020-2024 CERN
    SPDX-FileCopyrightText: 2023-2024 GSI Helmholtzzentrum für Schwerionenforschung
    SPDX-FileNotice: All rights not expressly granted are reserved.

    SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

Additional Renderers for RL
===========================

.. automodule:: cernml.rltools.renderers
    :members:
    :member-order: groupwise
    :exclude-members: Entry, from_callback
    :inherited-members: Renderer

.. autoclass:: cernml.rltools.renderers::RewardsLogRenderer.Entry
    :no-show-inheritance:
