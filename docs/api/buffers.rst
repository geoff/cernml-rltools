..
    SPDX-FileCopyrightText: 2020-2024 CERN
    SPDX-FileCopyrightText: 2023-2024 GSI Helmholtzzentrum für Schwerionenforschung
    SPDX-FileNotice: All rights not expressly granted are reserved.

    SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

Episode Buffers for RL Training
===============================

.. automodule:: cernml.rltools.buffers

.. autoclass:: StepTuple

.. data:: ActDType
    :type: typing.TypeVar

    The generic type variable for the `~numpy.dtype` of array-like
    `~gymnasium.Env` actions.

.. data:: ObsDType
    :type: typing.TypeVar

    The generic type variable for the `~numpy.dtype` of array-like
    `~gymnasium.Env` observations.

.. data:: DType
    :type: typing.TypeVar

    The generic type variable for custom `~numpy.dtype` overrides.
