..
    SPDX-FileCopyrightText: 2020-2024 CERN
    SPDX-FileCopyrightText: 2023-2024 GSI Helmholtzzentrum für Schwerionenforschung
    SPDX-FileNotice: All rights not expressly granted are reserved.

    SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

:tocdepth: 3

Uniform Command-Line Arguments for Cache Files
==============================================

.. automodule:: cernml.rltools.clicache
    :no-members:

Basic Interface
---------------

Users are not expected to need to use more than these two functions.

.. autofunction:: add_cache_control_group

.. autofunction:: train_or_load_module

Class-Based Interface
---------------------

These classes implement the logic of the basic interface. They may be
subclassed and extended if necessary.

.. autoclass:: TrainingCacher
    :special-members: __call__

.. autoclass:: TrainWhen
    :private-members: +_missing_

.. autoexception:: CacheError

.. autoexception:: NoCacheFileError

.. autoexception:: NoHandlerError

Compatibility with Other Packages
---------------------------------

Built-in Cache Plugins
^^^^^^^^^^^^^^^^^^^^^^

.. autofunction:: cernml.rltools.clicache.tf_plugin.tf_plugin

.. autofunction:: cernml.rltools.clicache.torch_plugin.torch_plugin

Decorators and types for Click
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. Explicitly list members so decorators are shown as decorators.

.. automodule:: cernml.rltools.clicache.click_plugin
    :no-members:

    .. autodecorator:: cache_control_options()

    .. autodecorator:: cache()

    .. autodecorator:: train_when()

    .. autoclass:: CacheFile

    .. autoclass:: TrainWhen
        :no-members:

Adding More Cache Handlers
--------------------------

.. currentmodule:: cernml.rltools.clicache

.. _entry points specification:
   https://packaging.python.org/en/latest/specifications/entry-points/

Framework selection uses the `entry points specification`_. If you want to add
support for new frameworks, you should write a `CachePlugin` function that
returns a `CacheHandler`. With this done, you can either register it to the
entry point group :ep:`cernml.rltools.clicache` or add it to the
`~TrainingCacher.plugins` list of a specific `TrainingCacher`.

.. entrypoint:: cernml.rltools.clicache

    The entry point group used by this package. Entry points are loaded via
    `load_entry_points()`. If an entry point cannot ve loaded (e.g. because it
    raises an exception or returns an object that isn't `callable`), it is
    ignored.

    If your entry point doesn't appear in the list, set the
    `TrainingCacher.logger` log `~logging.Logger.level` to `logging.DEBUG`.
    This should give you an idea why the entry point is being rejected.

.. autoclass:: CachePlugin

.. autoclass:: CacheHandler

.. autoclass:: NullHandler

.. autofunction:: load_entry_points

.. class:: Bypass(value: T, /)
    :canonical: cernml.rltools._bypass.BypassCache

    Tell `functools.cache` to ignore this argument.

    If you pass a custom logger to `load_entry_points()`, you have to wrap it
    in this marker class.

.. autoexception:: SkipCache
