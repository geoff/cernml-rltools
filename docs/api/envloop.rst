..
    SPDX-FileCopyrightText: 2020-2024 CERN
    SPDX-FileCopyrightText: 2023-2024 GSI Helmholtzzentrum für Schwerionenforschung
    SPDX-FileNotice: All rights not expressly granted are reserved.

    SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

RL Execution Loop Implementation
================================

.. automodule:: cernml.rltools.envloop
    :no-members:

Loops
-----

.. autofunction:: loop
.. autofunction:: collect_data
.. autofunction:: collect_random_data

Utilities
---------

.. autoclass:: Agent()

.. data:: ActType_co
    :type: typing.TypeVar

    The generic type variable for the actions of `Agent` and its subclasses.

.. data:: ObsType_contra
    :type: typing.TypeVar

    The generic type variable for the observations of `Agent` and its
    subclasses.

.. data:: StateType
    :type: typing.TypeVar
    :value: None

    The generic type variable for the state returned by `Agent` and its
    subclasses. This is only required for recurrent agents. Most agents carry
    no state between states and this type is simply None. This is the default.
