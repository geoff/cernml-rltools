# SPDX-FileCopyrightText: 2020-2024 CERN
# SPDX-FileCopyrightText: 2023-2024 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

"""Extension that lets you handle fixes for screwed-up cross-references."""

from __future__ import annotations

import dataclasses as dc
import time
import typing as t
from pathlib import PurePosixPath

from docutils.nodes import Element, TextElement, reference
from sphinx.util import logging, requests

if t.TYPE_CHECKING:
    from sphinx.addnodes import pending_xref
    from sphinx.application import Sphinx
    from sphinx.environment import BuildEnvironment
    from sphinx.util.typing import ExtensionMetadata

Uri = t.NewType("Uri", str)
Timestamp = t.NewType("Timestamp", int)
Duration = t.NewType("Duration", int)
Verified = t.NewType("Verified", bool)
RefTarget = t.NewType("RefTarget", str)
RefType = t.NewType("RefType", str)
VerifiedCacheData = t.NewType("VerifiedCacheData", "CacheData")

LOG = logging.getLogger(__name__)


def current_time() -> Timestamp:
    """Create timestamp of current time."""
    return Timestamp(int(time.time()))


def days(n: int, /) -> Duration:
    """Convert number of days into duration in seconds."""
    return Duration(int(n) * 86400)


@dc.dataclass(frozen=True)
class CacheData:
    """Data cached by the resolver."""

    reftarget: RefTarget
    reftype: RefType
    uri: Uri
    verified: bool


@dc.dataclass(frozen=True)
class CacheEntry(CacheData):
    """Full cache entry including its timestamp."""

    created: Timestamp

    def __init__(self, data: CacheData, created: Timestamp, /) -> None:
        super().__init__(
            reftarget=data.reftarget,
            reftype=data.reftype,
            uri=data.uri,
            verified=data.verified,
        )
        object.__setattr__(self, "created", created)


class Cache:
    """Wrapper around `BuildEnvironment` to implement a cache."""

    DEFAULT_LIFETIME: Duration = days(5)

    def __init__(
        self,
        env: BuildEnvironment,
        *,
        now: Timestamp | None = None,
        lifetime: Duration | None = DEFAULT_LIFETIME,
    ) -> None:
        self.now = now if now is not None else current_time()
        self.env = env
        self.env.tf2_resolver_cache = {}  # type: ignore[attr-defined]
        if lifetime is None:

            def is_expired(ts: Timestamp, /) -> bool:
                return False

        else:
            expiry_date = Timestamp(self.now - lifetime)

            def is_expired(ts: Timestamp, /) -> bool:
                return ts < expiry_date

        self.is_expired = is_expired

    def get(self, key: RefTarget) -> CacheData | None:
        """Retrieve data from the cache.

        This always checks the timestamp of the cache entry. If the
        entry has expired, it's removed and None is returned.
        """
        LOG.debug("retrieve from cache: %s", key)
        raw_entry = self._mapping.get(key)
        if not raw_entry:
            LOG.debug("not found")
            return None
        entry = CacheEntry(*raw_entry)
        LOG.debug("found: %r", entry)
        if entry and self.is_expired(entry.created):
            LOG.debug("cache entry is expired: %s", entry.created)
            self.delete(key)
            return None
        LOG.debug("still fresh!")
        return entry

    def set(self, key: RefTarget, data: CacheData) -> None:
        """Add a new cache entry."""
        entry = CacheEntry(data, self.now)
        LOG.debug("adding to cache: %r", entry)
        self._mapping[key] = dc.astuple(entry)

    def delete(self, key: RefTarget) -> None:
        """Delete a cache entry. Does nothing if there is no entry."""
        LOG.debug("removing from cache: %s", key)
        removed = self._mapping.pop(key, None)
        if removed:
            LOG.debug("evicted: %r", removed)

    def gc(self) -> None:
        """Delete all expired entries."""
        # Skip work if there is no expiry date.
        if not self.is_expired(Timestamp(0)):
            return
        LOG.info("collecting expired cache entries")
        expired_keys = set()
        mapping = self._mapping
        for key, raw_entry in mapping.items():
            entry = CacheEntry(*raw_entry)
            if self.is_expired(entry.created):
                expired_keys.add(key)
        LOG.info("%s/%s entries are stale", len(expired_keys), len(mapping))
        LOG.debug("stale entries: %r", expired_keys)
        for key in expired_keys:
            del mapping[key]
        LOG.info("GC finished")

    @property
    def _mapping(self) -> dict[str, tuple]:
        return self.env.tf2_resolver_cache  # type: ignore[attr-defined]


class Resolver:
    """Class for resolving and verifying URLs."""

    def __init__(self, base_uri: Uri, cache: Cache, *, verify_uris: bool) -> None:
        self.base_uri = base_uri.rstrip("/")
        self.cache = cache
        self.verify_uris = verify_uris

    def resolve(self, reftarget: RefTarget, reftype: RefType) -> Uri | None:
        """Resolve the given reference, possibly via the cache."""
        cached_data = self._get_from_cache(reftarget, reftype)
        data: VerifiedCacheData | None = (
            self._verify_uri(cached_data)
            if cached_data
            else self._resolve_uncached(reftarget, reftype)
        )
        if data:
            if not cached_data:
                self.cache.set(reftarget, data)
            return data.uri
        self.cache.delete(reftarget)
        return None

    def _get_from_cache(
        self, reftarget: RefTarget, reftype: RefType, /
    ) -> CacheData | None:
        """Wrapper around ``cache.get()`` that makes consistency checks."""
        data = self.cache.get(reftarget)
        if (
            data is not None
            and self._is_same_base(data.uri)
            and self._is_same_reftype(data.reftype, reftype)
        ):
            return data
        LOG.debug(
            "cached entry rejected as inconsistent: %s, %s, %s",
            self.base_uri,
            reftype,
            reftarget,
        )
        return None

    def _is_same_base(self, uri: Uri, /) -> bool:
        return uri.startswith(self.base_uri)

    @staticmethod
    def _is_same_reftype(cached: RefType, requested: RefType) -> bool:
        return cached == requested

    def _verify_uri(self, data: CacheData, /) -> VerifiedCacheData | None:
        if not self.verify_uris:
            LOG.debug("no verification requested")
            return VerifiedCacheData(data)
        if data.verified:
            LOG.debug("already verified")
            return VerifiedCacheData(data)
        try:
            LOG.debug("verifying: %s", data.uri)
            with requests.head(data.uri) as response:
                LOG.debug("status code: %d", response.status_code)
                response.raise_for_status()
        except requests.requests.HTTPError:
            LOG.exception(
                "ref :py:%s:`%s` is invalid: %s", data.reftype, data.reftarget, data.uri
            )
            return None
        LOG.debug("verified: %s", data.uri)
        return VerifiedCacheData(data)

    def _resolve_uncached(
        self, reftarget: RefTarget, reftype: RefType
    ) -> VerifiedCacheData | None:
        LOG.info("resolved :%s:`%s` ...", reftype, reftarget)
        uri = self._resolve_uri(reftarget, reftype)
        LOG.info("resolved: %s", uri)
        data = CacheData(reftarget, reftype, uri, verified=False)
        return self._verify_uri(data)

    def _resolve_uri(self, reftarget: RefTarget, reftype: RefType) -> Uri:
        path = make_path(reftarget, reftype)
        return Uri(self.base_uri + str(path))


def make_path(reftarget: RefTarget, reftype: RefType) -> PurePosixPath:
    """Resolve a node into a path relative to the base URI.

    Examples:
        >>> make_path('tf.version.VERSION', 'data')
        PurePosixPath('/tf/version#VERSION')
        >>> make_path('tensorflow.keras.Model', 'class')
        PurePosixPath('/tf/keras/Model')
    """
    if "/" in reftarget:
        raise ValueError(f"invalid target: {reftarget!r}")
    segments = reftarget.split(".")
    attr = segments.pop() if reftype in ("attr", "data", "meth") else ""
    if segments[0] == "tensorflow":
        segments[0] = "tf"
    path = PurePosixPath("/").joinpath(*segments)
    if attr:
        path = path.with_name(f"{path.name}#{attr}")
    return path


def matches(node: pending_xref) -> bool:
    """Return True if we're interested in this node."""
    return node["refdomain"] == "py" and any(
        map(node["reftarget"].startswith, ["tensorflow", "tf"])
    )


def generate_tf2_refs(
    app: Sphinx,
    env: BuildEnvironment,
    node: pending_xref,
    contnode: TextElement,
) -> Element | None:
    """Link type variables to `typing.TypeVar`."""
    if matches(node):
        lifetime = app.config.tf2_cache_lifetime
        cache = Cache(env, lifetime=days(lifetime) if lifetime is not None else None)
        resolver = Resolver(
            cache=cache,
            base_uri=Uri(str(app.config.tf2_base_url)),
            verify_uris=bool(app.config.tf2_verify_uris),
        )
        refuri = resolver.resolve(reftarget=node["reftarget"], reftype=node["reftype"])
        if refuri:
            return reference(
                node.rawsource,
                "",
                contnode,
                internal=False,
                reftitle="(in Tensorflow v2.x)",
                refuri=refuri,
            )
    return None


def setup(app: Sphinx) -> ExtensionMetadata:
    """Set up hooks into Sphinx."""
    app.setup_extension("sphinx.ext.intersphinx")
    app.add_config_value(
        name="tf2_base_url",
        default="https://www.tensorflow.org/api_docs/python",
        rebuild="env",
        types=str,
        description="base URL to link to",
    )
    app.add_config_value(
        name="tf2_cache_lifetime",
        default=5,
        rebuild="",
        types=[int, type(None)],
        description="time after which cached URLs should expire; "
        "set to None for no expiration",
    )
    app.add_config_value(
        name="tf2_verify_uris",
        default=True,
        rebuild="",
        types=bool,
        description="if true, each resolved URL will be tested via "
        "a HEAD request to its URL",
    )
    app.connect("missing-reference", generate_tf2_refs, priority=600)
    return {
        "version": "1.0",
        "parallel_read_safe": True,
        "parallel_write_safe": True,
    }
