#!/usr/bin/env python

# SPDX-FileCopyrightText: 2020 - 2024 CERN
# SPDX-FileCopyrightText: 2023 - 2024 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

"""Show-case the environment wrappers `LogRewards` and `RenderOnStep`."""

from __future__ import annotations

import gymnasium as gym
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from gymnasium.wrappers import TimeLimit
from numpy.typing import NDArray
from typing_extensions import override

from cernml.coi import InfoDict, Machine, SeparableEnv
from cernml.rltools.envloop import loop
from cernml.rltools.wrappers import LogRewards, RenderOnStep


class GoToZero2D(SeparableEnv[NDArray[np.double], NDArray[np.double]]):
    """Environment in which an agent must find the origin in 2D space."""

    metadata = {
        "render.modes": ["human"],
        "cern.machine": Machine.NO_MACHINE,
    }

    observation_space = gym.spaces.Box(-2, 2, (2,))
    action_space = gym.spaces.Box(-1, 1, (2,))
    reward_range = (-3.0, 0.0)
    reward_objective = -0.5

    def __init__(self, render_mode: str | None = None) -> None:
        self.render_mode = render_mode
        self.pos = np.zeros(self.observation_space.shape)
        xlims, ylims = np.array(
            [
                self.observation_space.low,
                self.observation_space.high,
            ]
        ).transpose()
        self.figure: mpl.figure.Figure | None = None
        if self.render_mode == "human":
            self.figure = plt.figure()
            axes = self.figure.add_subplot()
            axes.add_artist(
                mpl.patches.Circle(
                    (0, 0),
                    radius=abs(self.reward_objective),
                    fill=False,
                    linestyle="--",
                    color="black",
                )
            )
            axes.add_artist(
                mpl.patches.Rectangle(
                    tuple(self.observation_space.low),
                    *(self.observation_space.high - self.observation_space.low),
                    fill=False,
                    linestyle="--",
                    color="black",
                )
            )
            axes.plot(*self.pos, marker="o")
            axes.set_xlim(1.5 * xlims)
            axes.set_ylim(1.5 * ylims)

    @override
    def reset(
        self, *, seed: int | None = None, options: InfoDict | None = None
    ) -> tuple[NDArray[np.double], InfoDict]:
        super().reset(seed=seed, options=options)
        self.pos = self.observation_space.sample()
        if self.render_mode == "human":
            self.render()
        return self.pos.copy(), {}

    @override
    def compute_observation(
        self, action: NDArray[np.double], info: InfoDict
    ) -> NDArray[np.double]:
        self.pos += action
        return self.pos.copy()

    @override
    def compute_reward(
        self, obs: NDArray[np.double], goal: None, info: InfoDict
    ) -> float:
        return float(-np.linalg.norm(obs))

    @override
    def compute_terminated(
        self, obs: NDArray[np.double], reward: float, info: InfoDict
    ) -> bool:
        success = reward > self.reward_objective
        failure = obs not in self.observation_space
        if success or failure:
            info["success"] = success
        info["objective"] = self.reward_objective
        return success

    @override
    def compute_truncated(
        self, obs: NDArray[np.double], reward: float, info: InfoDict
    ) -> bool:
        failure = obs not in self.observation_space
        return failure

    @override
    def render(self) -> None:
        if self.render_mode == "human":
            assert self.figure is not None
            [axes] = self.figure.axes
            [lines] = axes.lines
            lines.set_data(*self.pos)
            self.figure.show()
            return
        super().render()


def main() -> None:
    """Main function."""
    env = RenderOnStep(LogRewards(TimeLimit(GoToZero2D(), 5)), timeout=0.01)
    loop(lambda obs: env.action_space.sample(), env, max_episodes=20)
    env.end_training()
    loop(lambda obs: env.action_space.sample(), env, max_episodes=10)
    plt.show()


if __name__ == "__main__":
    main()
