#!/usr/bin/env python

# SPDX-FileCopyrightText: 2020 - 2024 CERN
# SPDX-FileCopyrightText: 2023 - 2024 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

"""Simplified interface to the Intersphinx CLI."""

from __future__ import annotations

import argparse
import sys
import typing as t

if t.TYPE_CHECKING:
    from sphinx.ext.intersphinx._shared import IntersphinxMapping


def main() -> int:
    """Main function."""
    from docs import conf

    choices = list(conf.intersphinx_mapping)
    choice_string = ", ".join(map(repr, choices))
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "name",
        choices=choices,
        metavar="NAME",
        nargs="?",
        help=f"project to download the inventory for (choices: {choice_string})",
    )
    name: str | None = parser.parse_args().name
    if name is None:
        print("Choose one of:", choice_string, file=sys.stderr)
        return 1

    from sphinx.ext.intersphinx import inspect_main, validate_intersphinx_mapping

    validate_intersphinx_mapping(None, conf)  # type: ignore[arg-type]

    _, (url, targets) = t.cast("IntersphinxMapping", conf.intersphinx_mapping)[name]
    for target in targets:
        if target is None:
            target = url + "/objects.inv"
        if not (errcode := inspect_main([target])):
            return 0
    else:
        errcode = 1
    print("no inventory found", file=sys.stderr)
    return errcode


if __name__ == "__main__":
    sys.exit(main())
