<!--
SPDX-FileCopyrightText: 2020 - 2024 CERN
SPDX-FileCopyrightText: 2023 - 2024 GSI Helmholtzzentrum für Schwerionenforschung
SPDX-FileNotice: All rights not expressly granted are reserved.

SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+
-->

CERNML RL Tools
===============

This is a collection of useful classes and functions for machine learning that
have come up repeatedly while implementing automated control for the [AWAKE][]
experiment.

A strong effort is made to keep these helpers neutral w.r.t. the ML framework
that one uses (TensorFlow, PyTorch, Theano, etc.). Merge requests are welcome
whenever it is likely that a class or function is useful to multiple projects
and there is a benefit to having one central implementation.

The repository is available on CERN's [Gitlab][]. Its maintainer is @nmadysa
<nico.madysa@cern.ch>.

[AWAKE]: https://awake.web.cern.ch/
[Gitlab]: https://gitlab.cern.ch/geoff/cernml-rltools

Requirements
------------

This package only requires [NumPy][], [Matplotlib][] and [Gymnasium][],
as well as the [Common Optimization Interfaces][COI]. For details, see
[pyproject.toml].

[NumPy]: https://numpy.org/
[Matplotlib]: https://matplotlib.org/
[Gymnasium]: https://gymnasium.farama.org/
[COI]: https://gitlab.cern.ch/geoff/cernml-coi/
[pyproject.toml]: /pyproject.toml

License
-------

Except as otherwise noted, this work is licensed under either of [GNU Public
License, Version 3.0 or later](LICENSES/GPL-3.0-or-later.txt), or [European
Union Public License, Version 1.2 or later](LICENSES/EUPL-1.2.txt), at your
option. See [COPYING](COPYING) for details.

Unless You explicitly state otherwise, any contribution intentionally submitted
by You for inclusion in this Work (the Covered Work) shall be dual-licensed as
above, without any additional terms or conditions.

For full authorship information, see the version control history.
