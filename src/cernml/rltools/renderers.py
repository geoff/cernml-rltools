# SPDX-FileCopyrightText: 2020 - 2024 CERN
# SPDX-FileCopyrightText: 2023 - 2024 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

"""A variety of useful `FigureRenderer` subclasses."""

from __future__ import annotations

from dataclasses import dataclass

import matplotlib as mpl
import numpy as np
from matplotlib.figure import Figure

from cernml.mpl_utils import FigureRenderer

__all__ = ("RewardsLogRenderer",)


class RewardsLogRenderer(FigureRenderer):
    """Renderer that logs the outcome of each finished episode.

    This renderer produces a figure with two subplots. The first one
    shows the episode length and whether it was a success. The second
    one shows the final reward of each episode and, if possible, the
    zeroth reward and the reward objective.

    This is the renderer used internally by `.LogRewards`.

    Args:
        title: The figure title.
        render_mode: The render mode. Should be :rmode:`None` or
            :rmode:`"human"` or :rmode:`"matplotlib_figures"`.
    """

    @dataclass(init=False, frozen=False, eq=False, order=False)
    class Entry:
        """The `~dataclasses.dataclass` used by `RewardsLogRenderer.log`."""

        __slots__ = ("final", "initial", "length", "success", "threshold")

        initial: float
        """The :term:`initial reward` of an episode."""
        final: float
        """The most recent reward of the episode."""
        length: int
        """The number of steps in the episode."""
        success: bool | None
        """The :term:`success` state of the episode. True means success,
        False means failure, None indicates an ambiguous results."""
        threshold: float
        """The most recent :term:`reward objective` of the episode."""

        def __init__(self, initial: float) -> None:
            self.initial = initial
            self.final = initial
            self.length = 0
            self.success = None
            self.threshold = np.nan

    log: list[Entry]
    """The recorded data, one entry per episode. Every call to
    `start_next_episode()` appends a new entry."""

    figure: Figure

    def __init__(self, title: str, *, render_mode: str | None) -> None:
        super().__init__(title, render_mode=render_mode)
        self.log: list[RewardsLogRenderer.Entry] = []
        self.num_training = 0

    def start_next_episode(self, initial_reward: float = float("nan")) -> None:
        """Finalize the current episode and start a new one.

        Args:
            initial_reward: If passed, this is the :term:`initial
                reward` of the episode.
        """
        self.log.append(self.Entry(initial_reward))

    def update_current_episode(
        self, *, reward: float, success: bool | None = None, threshold: float = np.inf
    ) -> None:
        """Update the current episode with a new step.

        Args:
            reward: The reward received in this step.
            success: The :term:`success` state of the episode. This
                should only be passed if the episode has ended.
            threshold: If passed, this is the most recent :term:`reward
                objective` of the episode.
        """
        latest = self.log[-1]
        latest.length += 1
        latest.final = reward
        latest.success = success
        latest.threshold = threshold

    def end_training(self) -> None:
        """Mark the end of training.

        After this is called, the `figure` will highlight all episodes
        up to the current one in a different color.
        """
        self.num_training = len(self.log)

    def _init_figure(self, figure: Figure) -> None:
        ax_top: mpl.axes.Axes
        ax_bot: mpl.axes.Axes
        ax_top, ax_bot = figure.subplots(nrows=2, sharex=True, squeeze=False)
        span = ax_top.axvspan(
            0, 0, color="tab:green", alpha=0.2, label="Training phase"
        )
        ax_top.plot([], "-", zorder=0, color="tab:blue")
        ax_top.plot([], "o", zorder=1, color="tab:blue", label="Indeterminate")
        ax_top.plot([], "o", zorder=1, color="tab:green", label="Success")
        ax_top.plot([], "o", zorder=1, color="tab:orange", label="Failure")
        ax_top.legend(loc="lower left")
        # Only hide the span here so that it appears visible in the
        # legend.
        span.set_visible(False)
        ax_top.set_ylim(bottom=0.0, auto=True)
        ax_top.set_ylabel("Episode length")
        ax_bot.plot([], "o-", label="Initial")
        ax_bot.plot([], "o-", label="Final")
        ax_bot.plot([], "k--", label="Objective")
        ax_bot.legend(loc="lower left")
        ax_bot.set_ylabel("Reward")
        ax_bot.set_xlabel("Episode")
        self._update_figure(figure)

    def _update_figure(self, figure: Figure) -> None:
        ax_top, ax_bot = figure.axes
        self._update_training_span(ax_top)
        # Before plotting, check that at least one episode has finished.
        if self.log:
            self._update_length_plots(ax_top)
            self._update_reward_plots(ax_bot)
            ax_top.relim()
            ax_bot.relim()
            ax_top.autoscale_view()
            ax_bot.autoscale_view()
            ax_top.set_ylim(bottom=0.0, auto=True)

    def _update_training_span(self, ax_top: mpl.axes.Axes) -> None:
        [training_span] = ax_top.patches
        assert isinstance(training_span, mpl.patches.Polygon), training_span
        training_span.set_visible(bool(self.num_training))
        _update_span(training_span, -0.5, self.num_training - 0.5)

    def _update_length_plots(self, ax_top: mpl.axes.Axes) -> None:
        [l_all, l_indet, l_success, l_failure] = ax_top.lines
        indices = np.arange(len(self.log))
        lengths = np.array([entry.length for entry in self.log])
        indeterminates = np.array([entry.success is None for entry in self.log])
        successes = np.array([bool(entry.success) for entry in self.log])
        failures = ~indeterminates & ~successes
        l_all.set_data(indices, lengths)
        l_indet.set_data(indices[indeterminates], lengths[indeterminates])
        l_success.set_data(indices[successes], lengths[successes])
        l_failure.set_data(indices[failures], lengths[failures])

    def _update_reward_plots(self, ax_bot: mpl.axes.Axes) -> None:
        indices = np.arange(len(self.log))
        [l_initial, l_final, l_threshold] = ax_bot.lines
        initials = np.array([entry.initial for entry in self.log])
        finals = np.array([entry.final for entry in self.log])
        thresholds = np.array([entry.threshold for entry in self.log])
        l_initial.set_data(indices, initials)
        l_final.set_data(indices, finals)
        l_threshold.set_data(indices, thresholds)


def _update_span(span: mpl.patches.Polygon, low: float, high: float) -> None:
    path = np.array([[low, 0.0], [low, 1.0], [high, 1.0], [high, 0.0], [low, 0.0]])
    span.set_xy(path)
