# SPDX-FileCopyrightText: 2020 - 2024 CERN
# SPDX-FileCopyrightText: 2023 - 2024 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

"""Backport of `enum.StrEnum` from Python 3.11."""

import enum
import typing as t

from typing_extensions import Self, override

__all__ = ("StrEnum",)


class StrEnum(str, enum.Enum):
    """Enum where members are also (and must be) strings.

    This has been copied and lightly edited from the Python 3.11 `enum`
    module.
    """

    def __new__(cls, *values: t.Any) -> Self:
        """Values must already be of type `str`."""
        if len(values) > 3:
            raise TypeError(f"too many arguments for str(): {values!r}")
        if len(values) == 1 and not isinstance(values[0], str):
            raise TypeError(f"{values[0]!r} is not a string")
        if len(values) >= 2 and not isinstance(values[1], str):
            raise TypeError(f"encoding must be a string, not {values[1]!r}")
        if len(values) == 3 and not isinstance(values[2], str):
            raise TypeError(f"errors must be a string, not {values[2]!r}")
        value = str(*values)
        member = str.__new__(cls, value)
        member._value_ = value
        return member

    @override
    @staticmethod
    def _generate_next_value_(
        name: str, start: int, count: int, last_values: list
    ) -> t.Any:
        return name.lower()

    __str__ = str.__str__


try:
    from enum import StrEnum  # type: ignore[assignment]
except ImportError:
    pass
