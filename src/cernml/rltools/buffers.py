# SPDX-FileCopyrightText: 2020 - 2024 CERN
# SPDX-FileCopyrightText: 2023 - 2024 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

"""Provides a buffer of steps taken by an agent in a Gym environment.

This module provides `StepBuffer`, which stores the full information of
individual time steps through an environment. This can be used e.g. to
train a substitute model for the environment.

There is also a bounded-size variant, `LimitedStepBuffer`.

Finally, `RecordSteps` is a wrapper that automatically fills a step
buffer for you.
"""

from __future__ import annotations

import typing as t

import numpy as np
from gymnasium import Env, Wrapper
from typing_extensions import NamedTuple, Self, override

if t.TYPE_CHECKING:
    from numpy.typing import NDArray

    from cernml.coi import InfoDict


__all__ = (
    "StepBuffer",
    "LimitedStepBuffer",
    "RecordSteps",
    "Step",
    "StepTuple",
    "ActDType",
    "ObsDType",
)


ObsDType = t.TypeVar("ObsDType", bound=np.generic)
ActDType = t.TypeVar("ActDType", bound=np.generic)
DType = t.TypeVar("DType", bound=np.generic)

if not t.TYPE_CHECKING:
    # Fake NDArray because we can't always use forward references.
    class NDArray(t.Generic[DType]):
        """Dummy class to force autodoc to parse `RecordSteps`'s bases correctly."""

        __qualname__ = "numpy.typing.NDArray"
        __module__ = "numpy.typing"
        __slots__ = ()


# Note: We use the NamedTuple from typing_extensions because on Python
# 3.9, it didn't support Generic yet.
class Step(NamedTuple, t.Generic[ObsDType, ActDType]):
    """Element type of `StepBuffer`."""

    obs: NDArray[ObsDType]
    """The observation before this step."""
    action: NDArray[ActDType]
    """The action taken by the agent."""
    reward: float
    """The reward for this action."""
    next_obs: NDArray[ObsDType]
    """The observation after this step."""
    terminated: bool
    """The episode has reached its end."""
    truncated: bool
    """The episode has been cancelled, e.g. by a time limit."""


StepTuple: t.TypeAlias = tuple[
    NDArray[ObsDType],
    NDArray[ActDType],
    t.SupportsFloat,
    NDArray[ObsDType],
    bool,
    bool,
]


def _make_step(
    obs: NDArray[ObsDType],
    action: NDArray[ActDType],
    reward: t.SupportsFloat,
    next_obs: NDArray[ObsDType],
    terminated: bool,
    truncated: bool,
) -> Step[ObsDType, ActDType]:
    """Create a `Step` with all attributes copied and coerced."""
    # pylint: disable = too-many-arguments
    obs = np.array(obs, copy=True)
    action = np.array(action, copy=True)
    reward = float(reward)
    next_obs = np.array(next_obs, copy=True)
    terminated = bool(terminated)
    truncated = bool(truncated)
    for array in obs, action, next_obs:
        array.setflags(write=False)
    return Step(obs, action, reward, next_obs, terminated, truncated)


class StepBuffer(t.Sequence[Step[ObsDType, ActDType]]):
    """A buffer of steps taken by an agent through a Gym environment.

    This is a trivial storage class without much logic of its own. Use
    it with `RecordSteps` to automatically record the steps of an RL
    agent. This can be used to e.g. train a substitute model for an
    environment.

    Given this example environment:

        >>> import gymnasium as gym
        >>> from gymnasium.wrappers import TimeLimit
        >>> class SimpleEnv(gym.Env):
        ...     observation_space = gym.spaces.Box(-1, 1, shape=(2,))
        ...     action_space = gym.spaces.Box(-1, 1, shape=(2,))
        ...
        ...     def __init__(self):
        ...         self.pos = None
        ...
        ...     def reset(self, *, seed=None, options=None):
        ...         super().reset(seed=seed, options=options)
        ...         self.pos = self.observation_space.sample()
        ...         return self.pos.copy(), {}
        ...
        ...     def step(self, action):
        ...         self.pos += action
        ...         dist = sum(self.pos**2)
        ...         return self.pos.copy(), -dist, False, False, {}

    `StepBuffer` can be filled like this:

        >>> env = RecordSteps(TimeLimit(SimpleEnv(), 4))
        >>> for _ in range(10):
        ...     obs, _ = env.reset()
        ...     done = False
        ...     while not done:
        ...         action = env.action_space.sample()
        ...         obs, _, terminated, truncated, _ = env.step(action)
        ...         done = terminated or truncated
        >>> buf = env.step_buffer
        >>> buf
        <StepBuffer of 40 elements>

    It grants access to rows and columns of its data:

        >>> buf.get_obs().shape
        (40, 2)
        >>> buf.get_action().shape
        (40, 2)
        >>> buf.get_reward().shape
        (40,)
        >>> all(isinstance(item, Step) for item in buf)
        True
        >>> np.array_equal(buf.get_obs()[1:4], buf.get_next_obs()[:3])
        True
        >>> int(sum(buf.get_truncated()))
        10

    A `StepBuffer` can be shuffled and sampled for simplified data
    access:

        >>> import random
        >>> random.shuffle(buf)
        >>> len(random.sample(buf, 10))
        10
        >>> import numpy
        >>> numpy.random.shuffle(buf)
        >>> len(buf[numpy.random.choice(len(buf), 10)])
        10

    It can be sliced for easy splitting into training/validation data:

        >>> isplit = int(0.75 * len(buf))
        >>> buf[:isplit], buf[isplit:]
        (<StepBuffer of 30 elements>, <StepBuffer of 10 elements>)

    They can also be copied:

        >>> copy1 = StepBuffer(buf)
        >>> copy2 = buf.copy()
        >>> copy1 is not buf is not copy2
        True
        >>> len(buf) == len(copy1) == len(copy2)
        True
        >>> all(left == right for left, right in zip(buf, copy1))
        True
        >>> all(left == right for left, right in zip(buf, copy2))
        True
        >>> copy1.clear()
        >>> del copy2[:]
        >>> len(copy1) == len(copy2) == 0
        True
    """

    # pylint: disable = too-many-ancestors

    def __init__(
        self, other: t.Iterable[Step[ObsDType, ActDType]] | None = None
    ) -> None:
        self._buffer: list[Step]
        if other is None:
            self._buffer = []
        elif isinstance(other, type(self)):
            self._buffer = list(other._buffer)
        else:
            other = list(other)
            if not all(isinstance(item, Step) for item in other):
                raise TypeError("not an iterable of steps: " + repr(other))
            self._buffer = other

    def __repr__(self) -> str:
        return f"<{type(self).__name__} of {len(self)} elements>"

    def __len__(self) -> int:
        return len(self._buffer)

    def __iter__(self) -> t.Iterator[Step[ObsDType, ActDType]]:
        return iter(self._buffer)

    def __reversed__(self) -> t.Iterator[Step[ObsDType, ActDType]]:
        return reversed(self._buffer)

    def __contains__(self, item: t.Any) -> bool:
        return item in self._buffer

    @t.overload
    def __getitem__(self, key: int) -> Step[ObsDType, ActDType]: ...

    @t.overload
    def __getitem__(self, key: slice) -> StepBuffer[ObsDType, ActDType]: ...

    @t.overload
    def __getitem__(self, key: t.Sequence[int]) -> StepBuffer[ObsDType, ActDType]: ...

    def __getitem__(
        self, key: int | slice | t.Sequence[int]
    ) -> Step[ObsDType, ActDType] | StepBuffer[ObsDType, ActDType]:
        if isinstance(key, (int, np.integer)):
            return self._buffer[key]
        if isinstance(key, slice):
            result = StepBuffer[ObsDType, ActDType]()
            result._buffer = self._buffer[key]
            return result
        # Handle arrays and lists as keys.
        _assert_index_list(key)
        result = StepBuffer()
        result._buffer = [self._buffer[i] for i in key]
        return result

    @t.overload
    def __setitem__(self, key: int, value: Step[ObsDType, ActDType]) -> None: ...

    @t.overload
    def __setitem__(
        self, key: slice, value: t.Iterable[Step[ObsDType, ActDType]]
    ) -> None: ...

    @t.overload
    def __setitem__(
        self, key: t.Sequence[int], value: t.Iterable[Step[ObsDType, ActDType]]
    ) -> None: ...

    def __setitem__(
        self,
        key: int | slice | t.Sequence[int],
        value: Step[ObsDType, ActDType] | t.Iterable[Step[ObsDType, ActDType]],
    ) -> None:
        # Handle integer keys.
        if isinstance(key, (int, np.integer)):
            if not isinstance(value, Step):
                raise TypeError("not a step: " + repr(value))
            self._buffer[key] = value
            return
        # Coerce one-shot iterators to sequence for type checking.
        values_list = list(t.cast(t.Iterable[Step], value))
        if not all(isinstance(item, Step) for item in value):
            raise TypeError("not an iterable of steps: " + repr(value))
        # Handle slices.
        if isinstance(key, slice):
            self._buffer[key] = values_list
            return
        # Handle array/list keys.
        _assert_index_list(key)
        if len(values_list) != len(key):
            raise ValueError(
                f"shape mismatch: assigning {len(values_list)} values "
                f"to {len(key)} elements of a buffer"
            )
        for k, item in zip(key, values_list):
            self._buffer[k] = item

    def __delitem__(self, key: int | slice | t.Sequence[int]) -> None:
        # Handle integer keys.
        if isinstance(key, (int, np.integer, slice)):
            del self._buffer[key]
            return
        # Handle array/list keys.
        _assert_index_list(key)
        for k in key:
            del self._buffer[k]

    def clear(self) -> None:
        """Delete all time steps for this buffer."""
        self._buffer.clear()

    def copy(self) -> Self:
        """Return a shallow copy of the buffer."""
        return type(self)(self)

    def append(
        self,
        obs: NDArray[ObsDType],
        action: NDArray[ActDType],
        reward: t.SupportsFloat,
        next_obs: NDArray[ObsDType],
        terminated: bool,
        truncated: bool,
    ) -> None:
        """Add a time step to the buffer.

        Args:
            obs: The observation before the action has been applied.
            action: The action that transitions the environment.
            reward: The reward for the action.
            next_obs: The observation after the action has been applied.
            done: Whether the action ended the current episode.
        """
        # pylint: disable=too-many-arguments
        self._buffer.append(
            _make_step(obs, action, reward, next_obs, terminated, truncated)
        )

    def extend(
        self,
        items: t.Iterable[Step[ObsDType, ActDType] | StepTuple[ObsDType, ActDType]],
    ) -> None:
        r"""Add multiple time steps to the buffer.

        The given iterable should either be another step buffer, an
        iterable of `~cernml.rltools.buffers.Step`\ s or an interable of
        `tuple <cernml.rltools.buffers.StepTuple>`\ s that can be
        converted to steps.
        """
        if isinstance(items, type(self)):
            self._buffer.extend(items._buffer)  # pylint: disable=protected-access
        else:
            self._buffer.extend(_make_step(*args) for args in items)

    @t.overload
    def get_obs(self, dtype: None = None) -> NDArray[ObsDType]: ...
    @t.overload
    def get_obs(self, dtype: type[DType]) -> NDArray[DType]: ...
    def get_obs(self, dtype: type[DType] | None = None) -> NDArray[DType]:
        """Return an array of the initial observations in the buffer."""
        return np.array([step.obs for step in self._buffer], dtype=dtype)

    @t.overload
    def get_action(self, dtype: None = None) -> NDArray[ActDType]: ...
    @t.overload
    def get_action(self, dtype: type[DType]) -> NDArray[DType]: ...
    def get_action(self, dtype: type[DType] | None = None) -> NDArray[DType]:
        """Return an array of the actions in the buffer."""
        return np.array([step.action for step in self._buffer], dtype=dtype)

    @t.overload
    def get_reward(self, dtype: None = None) -> NDArray[np.double]: ...
    @t.overload
    def get_reward(self, dtype: type[DType]) -> NDArray[DType]: ...
    def get_reward(self, dtype: type[DType] | None = None) -> NDArray[DType]:
        """Return an array of the rewards in the buffer."""
        return np.array([step.reward for step in self._buffer], dtype=dtype)

    @t.overload
    def get_next_obs(self, dtype: None = None) -> NDArray[ObsDType]: ...
    @t.overload
    def get_next_obs(self, dtype: type[DType]) -> NDArray[DType]: ...
    def get_next_obs(self, dtype: type[DType] | None = None) -> NDArray[DType]:
        """Return an array of the final observations in the buffer."""
        return np.array([step.next_obs for step in self._buffer], dtype=dtype)

    @t.overload
    def get_terminated(self, dtype: None = None) -> NDArray[np.bool_]: ...
    @t.overload
    def get_terminated(self, dtype: type[DType] | None = None) -> NDArray[DType]: ...
    def get_terminated(self, dtype: type[DType] | None = None) -> NDArray[DType]:
        """Return an array of the terminated flags in the buffer."""
        return np.array([step.terminated for step in self._buffer], dtype=dtype)

    @t.overload
    def get_truncated(self, dtype: None = None) -> NDArray[np.bool_]: ...
    @t.overload
    def get_truncated(self, dtype: type[DType] | None = None) -> NDArray[DType]: ...
    def get_truncated(self, dtype: type[DType] | None = None) -> NDArray[DType]:
        """Return an array of the truncated flags in the buffer."""
        return np.array([step.truncated for step in self._buffer], dtype=dtype)


class LimitedStepBuffer(StepBuffer[ObsDType, ActDType]):
    """A StepBuffer of limited size.

    When constructing this buffer, you pass the maximum size. If you
    don't, it attempts to copy it from the *other* object.

        >>> from collections import deque
        >>> buf = LimitedStepBuffer(maxlen=3)
        >>> LimitedStepBuffer(buf).maxlen
        3
        >>> LimitedStepBuffer(deque(maxlen=3)).maxlen
        3

    When appending steps beyond the maximum size, this buffer starts
    overwriting old entries, starting at the oldest:

        >>> buf.append(0, 0, 0, 0, False, False)
        >>> buf.append(1, 1, 1, 1, False, False)
        >>> buf.append(2, 2, 2, 2, False, False)
        >>> buf.append(3, 3, 3, 3, False, False)
        >>> buf.get_obs()
        array([3, 1, 2])

    This also works at construction time:

        >>> smaller = LimitedStepBuffer(buf, maxlen=2)
        >>> smaller.get_obs()
        array([2, 1])
        >>> smaller.extend(buf)
        >>> smaller.get_obs()
        array([1, 2])
    """

    # pylint: disable = too-many-ancestors

    def __init__(
        self,
        other: t.Iterable[Step[ObsDType, ActDType]] | None = None,
        maxlen: int | None = None,
    ) -> None:
        super().__init__(other)
        if maxlen is None:
            maxlen = getattr(other, "maxlen", None)
        if maxlen is None:
            self._buffer = []
            raise TypeError("__init__() missing required argument: 'maxlen'") from None
        if maxlen <= 0:
            self._buffer = []
            raise ValueError("maxlen must be positive: " + repr(maxlen))
        self._append_ptr = 0
        self._maxlen = maxlen
        if len(self) > self.maxlen:
            self._buffer, excess = self._buffer[:maxlen], self._buffer[maxlen:]
            self.extend(excess)

    @t.overload
    def __setitem__(self, key: int, value: Step[ObsDType, ActDType]) -> None: ...

    @t.overload
    def __setitem__(
        self, key: slice, value: t.Iterable[Step[ObsDType, ActDType]]
    ) -> None: ...

    @t.overload
    def __setitem__(
        self, key: t.Sequence[int], value: t.Iterable[Step[ObsDType, ActDType]]
    ) -> None: ...

    def __setitem__(self, key: t.Any, value: t.Any) -> None:
        if isinstance(key, slice):
            raise TypeError(
                f"buffer indices must be integers or arrays "
                f"of indices, not {type(key)}"
            )
        super().__setitem__(key, value)

    @property
    def maxlen(self) -> int:
        """The maximum length of the buffer."""
        return self._maxlen

    @override
    def append(
        self,
        obs: NDArray[ObsDType],
        action: NDArray[ActDType],
        reward: t.SupportsFloat,
        next_obs: NDArray[ObsDType],
        terminated: bool,
        truncated: bool,
    ) -> None:
        # pylint: disable=too-many-arguments
        if len(self._buffer) < self._maxlen:
            super().append(obs, action, reward, next_obs, terminated, truncated)
        else:
            self._buffer[self._append_ptr] = _make_step(
                obs, action, reward, next_obs, terminated, truncated
            )
            self._append_ptr = (self._append_ptr + 1) % self._maxlen

    @override
    def extend(
        self,
        items: t.Iterable[Step[ObsDType, ActDType] | StepTuple[ObsDType, ActDType]],
    ) -> None:
        # Get a checked or unchecked iterator.
        if isinstance(items, StepBuffer):
            iterator = iter(items._buffer)  # pylint: disable=protected-access
        else:
            iterator = (_make_step(*args) for args in items)
        # First, try to fill the buffer.
        while len(self._buffer) < self._maxlen:
            item = next(iterator, None)
            if item is None:
                return
            self._buffer.append(item)
        # Then start overwriting elements.
        for item in iterator:
            self._buffer[self._append_ptr] = item
            self._append_ptr = (self._append_ptr + 1) % self._maxlen


def _assert_index_list(key: int | slice | t.Sequence[int]) -> None:
    """Ensure that `key` is a list or array of integers."""
    if isinstance(key, list) and all(isinstance(i, (int, np.integer)) for i in key):
        return
    if isinstance(key, np.ndarray) and key.dtype == int:
        return
    raise TypeError(
        f"buffer indices must be integers, slices, or arrays "
        f"of indices, not {type(key)}"
    )


class RecordSteps(
    Wrapper[NDArray[ObsDType], NDArray[ActDType], NDArray[ObsDType], NDArray[ActDType]],
    t.Generic[ObsDType, ActDType],
):
    """Environment wrapper that stores each step in a `StepBuffer`.

    Args:
        env: The environment to wrap.
        step_buffer: If passed and not None, the buffer to use for
            storage. Otherwise, a new buffer is instantiated.
    """

    step_buffer: StepBuffer[ObsDType, ActDType]
    """The buffer into which steps are being stored."""
    last_obs: NDArray[ObsDType] | None
    """The observation returned by the last call to
    :func:`~gymnasium.Env.reset()` or :func:`~gymnasium.Env.step()`."""

    def __init__(
        self,
        env: Env[NDArray[ObsDType], NDArray[ActDType]],
        step_buffer: StepBuffer[ObsDType, ActDType] | None = None,
    ) -> None:
        super().__init__(env)
        if step_buffer is None:
            step_buffer = StepBuffer()
        self.step_buffer = step_buffer
        self.last_obs = None

    @override
    def reset(
        self, *, seed: int | None = None, options: InfoDict | None = None
    ) -> tuple[NDArray[ObsDType], InfoDict]:
        """Forward to wrapped env and update `last_obs`, but store nothing."""
        obs, info = super().reset(seed=seed, options=options)
        self.last_obs = obs.copy()
        return obs, info

    @override
    def step(
        self, action: NDArray[ActDType]
    ) -> tuple[NDArray[ObsDType], t.SupportsFloat, bool, bool, InfoDict]:
        """Forward to wrapped env and record the step data."""
        if self.last_obs is None:
            raise RuntimeError("step() called before reset()")
        obs, reward, terminated, truncated, info = super().step(
            np.array(action, copy=True)
        )
        self.step_buffer.append(
            self.last_obs, action, reward, obs, terminated, truncated
        )
        self.last_obs = obs.copy()
        return obs, reward, terminated, truncated, info


# Remove the mock NDArray to not confuse autodoc.
if not t.TYPE_CHECKING:
    del NDArray
