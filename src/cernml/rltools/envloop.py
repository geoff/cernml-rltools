# SPDX-FileCopyrightText: 2020 - 2024 CERN
# SPDX-FileCopyrightText: 2023 - 2024 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

"""Simple evaluation loop for Gym environments.

This module provides several :ref:`execution loops
<coi:guide/core:Running your optimization problem>` for `.Env`
subclasses: `loop()` as the most general form; as well as
`collect_data()` and `collect_random_data()`, which integrate with the
`.buffers` module.

It also defines `Agent` as a succinct description of the API expected by
the execution loops. This API has been chosen such that all :doc:`Stable
Baselines <sb3:modules/base>` satisfy it.

For convenience, this module also exports `.RecordSteps`, `.StepBuffer` and
`.LimitedStepBuffer` from the `.buffers` module.
"""

from __future__ import annotations

import typing as t

from gymnasium import Env
from gymnasium.wrappers import TimeLimit
from typing_extensions import TypeVar, override

from .buffers import ActDType, LimitedStepBuffer, ObsDType, RecordSteps, StepBuffer

if t.TYPE_CHECKING:
    from numpy.typing import NDArray

    from cernml.coi import InfoDict

__all__ = (
    "loop",
    "collect_data",
    "collect_random_data",
    "Agent",
    "ActType_co",
    "ObsType_contra",
    "StateType",
    "StepBuffer",
    "LimitedStepBuffer",
    "RecordSteps",
)


ObsType_contra = TypeVar("ObsType_contra", contravariant=True)
ActType_co = TypeVar("ActType_co", covariant=True)


def loop(
    agent: (
        Agent[ObsType_contra, ActType_co, t.Any]
        | t.Callable[[ObsType_contra], ActType_co]
    ),
    env: Env[ObsType_contra, ActType_co] | None = None,
    *,
    seed: int | None = None,
    options: InfoDict | None = None,
    max_episodes: int | None = None,
    max_timesteps: int | None = None,
) -> None:
    """Run an agent and an environment in a predict–step loop.

    This is a convenience function that simply simulates the system of
    agent and environments for a given amount of time. It returns
    nothing. To collect data, consider using `collect_data()`,
    `collect_random_data()` or a custom `~gymnasium.Wrapper`.

    Args:
        agent: An RL agent. Either a callable :samp:`def({obs}) ->
            {action}`, or an `Agent` that adheres to the :doc:`Stable
            Baselines API <sb3:modules/base>`.
        env: If passed and not None, a `Env` instance to run the agent
            on. Otherwise, the attribute :samp:`{agent}.env` is assumed
            to exist and used instead.

    Keyword Args:
        seed: If passed, it's forwarded to the first (and only the
            first) call to `reset() <gymnasium.Env.reset>`.
        options: If passed, it's forwarded to the first (and only the
            first) call to `reset() <gymnasium.Env.reset>`.
        max_episodes: Run the loop for this many episodes.
        max_timesteps: Run the loop for this many steps.

    If neither *max_episodes* nor *max_timesteps* is passed, the loop
    runs for one episode. If both are passed, the loop exits if either
    one of the two conditions are fulfilled.

    Raises:
        ValueError: if both *env* and *agent.env* are None.

    ..
        >>> from unittest.mock import Mock
        >>> Env = Mock()
        >>> x = Env.reset.return_value
        >>> Env.reset.return_value = (x.obs, x.info)
        >>> x = Env.step.return_value
        >>> Env.step.return_value = (x.obs, x.reward, x.terminated, x.truncated, x.info)

    Examples:
        >>> class ExampleEnv(Env): ...
        ...
        >>> class ExampleAgent:
        ...     def __init__(self, env):
        ...         self.env = env
        ...
        ...     def predict(self, observation, state=None):
        ...         return self.env.action_space.sample(), None
        ...
        >>> env = ExampleEnv()
        >>> agent = ExampleAgent(env)
        >>> loop(agent)  # Same as max_episodes=1.
        >>> loop(agent, max_episodes=10)
        >>> loop(agent, max_timesteps=100)
        >>> some_function = lambda obs: env.action_space.sample()
        >>> loop(some_function, env)  # Use function instead of class.

    """
    # pylint: disable = too-many-arguments

    # Normalize `predict()`.
    if isinstance(agent, Agent):
        predict = agent.predict
    elif callable(agent):
        predict = _FuncAgent(agent).predict
    else:
        raise TypeError("agent has no predict() and is not callable") from None
    env = _normalize_env(agent, env)
    # Normalize run conditions.
    if max_episodes is None and max_timesteps is None:
        max_episodes = 1
    i_episodes = 0
    i_timesteps = 0
    state = None
    terminated = truncated = False
    if i_timesteps == max_timesteps or i_episodes == max_episodes:
        return
    obs, _ = env.reset(seed=seed, options=options)
    # integers never compare equal to `None`, hence this loop condition
    # works as expected even if the `max_*` arguments are None.
    while i_timesteps != max_timesteps and i_episodes != max_episodes:
        if terminated or truncated:
            obs, _ = env.reset()
        action, state = predict(obs, state)
        obs, _, terminated, truncated, _ = env.step(action)
        i_timesteps += 1
        if terminated or truncated:
            i_episodes += 1


def collect_data(
    agent: (
        Agent[NDArray[ObsDType], NDArray[ActDType], t.Any]
        | t.Callable[[NDArray[ObsDType]], NDArray[ActDType]]
    ),
    env: Env[NDArray[ObsDType], NDArray[ActDType]] | None = None,
    *,
    max_episodes: int | None = None,
    max_timesteps: int | None = None,
    step_buffer: StepBuffer | None = None,
) -> StepBuffer[ObsDType, ActDType]:
    """Collect steps of an agent in an environment.

    This is a thin wrapper around `loop()` that collects each individual
    step in a `.StepBuffer`. Most arguments are the same as for `loop()`.
    See there for usage examples.

    Args:
        agent: An RL agent. Either a callable :samp:`def({obs}) ->
            {action}`, or an `Agent` that adheres to the :doc:`Stable
            Baselines API <sb3:modules/base>`.
        env: If passed and not None, a `Env` instance to run the agent
            on. Otherwise, the attribute :samp:`{agent}.env` is assumed
            to exist and used instead.

    Keyword Args:
        max_episodes: Run the loop for this many episodes.
        max_timesteps: Run the loop for this many steps.
        step_buffer: If passed and not None, append collected steps to
            this `.StepBuffer` instance. By default, a new buffer is
            created.

    Returns:
        The `.StepBuffer` into which data has been collected.

    Raises:
        ValueError: if both *env* and *agent.env* are None.
    """
    env = _normalize_env(agent, env)
    env = RecordSteps(env, step_buffer)
    loop(agent, env, max_episodes=max_episodes, max_timesteps=max_timesteps)
    return env.step_buffer


@t.overload
def collect_random_data(
    env: Env[NDArray[ObsDType], NDArray[ActDType]],
    max_episodes: int,
    /,
    *,
    step_buffer: StepBuffer[ObsDType, ActDType] | None = None,
) -> StepBuffer[ObsDType, ActDType]: ...


@t.overload
def collect_random_data(
    env: Env[NDArray[ObsDType], NDArray[ActDType]],
    *,
    max_episode_length: int | None = 1,
    max_episodes: int | None = None,
    max_timesteps: int | None = None,
    step_buffer: StepBuffer[ObsDType, ActDType] | None = None,
) -> StepBuffer[ObsDType, ActDType]: ...


def collect_random_data(
    env: Env[NDArray[ObsDType], NDArray[ActDType]],
    *args: int,
    max_episode_length: int | None = 1,
    max_episodes: int | None = None,
    max_timesteps: int | None = None,
    step_buffer: StepBuffer[ObsDType, ActDType] | None = None,
) -> StepBuffer[ObsDType, ActDType]:
    """Collect random steps in an environment.

    This function is similar to `collect_data()` called with an agent
    that picks actions at random. Actions are chosen by calling
    :samp:`{env}.action_space.sample()`. In addition, the API is
    slightly different.

    Because a random agent generally has no chance to solve an
    environment, the environment always has to be time-limited.

    Args:
        env: An `Env` instance to run the random agent on. Unlike with
            `loop()` and `collect_data()`, this parameter is required.
        max_episodes: Run the loop for this many episodes. This may only
            be passed as a *positional* argument if none of the other
            maximums are passed.

    Keyword Args:
        max_episode_lengths: The mandatory time limit on each episode.
            The default is to run only one step per episode.
        max_episodes: Run the loop for this many episodes.
        max_timesteps: Run the loop for this many steps.
        step_buffer: If passed and not None, append collected steps to
            this `.StepBuffer` instance. By default, a new buffer is
            created.

    Returns:
        The `.StepBuffer` into which data has been collected.
    """
    # If the max_* kwargs are not passed, we allow passing max_episodes
    # as a positional argument.
    if args:
        kwargs_are_default: bool = (
            max_episode_length == 1 and max_episodes is None and max_timesteps is None
        )
        if kwargs_are_default:
            try:
                (max_episodes,) = args
            except ValueError:
                raise TypeError(
                    f"collect_random_data() takes 0 to 1 positional "
                    f"arguments, but {len(args)} "
                    f"{'were' if len(args) != 1 else 'was'} given"
                ) from None
        else:
            raise TypeError(
                f"collect_random_data() takes 0 positional "
                f"arguments, but {len(args)} "
                f"{'were' if len(args) != 1 else 'was'} given"
            ) from None
    if max_episode_length is not None:
        env = TimeLimit(env, max_episode_length)
    env = RecordSteps(env, step_buffer)
    loop(
        lambda _: env.action_space.sample(),
        env,
        max_episodes=max_episodes,
        max_timesteps=max_timesteps,
    )
    return env.get_wrapper_attr("step_buffer")


StateType = TypeVar("StateType", default=None)


@t.runtime_checkable
class Agent(t.Protocol[ObsType_contra, ActType_co, StateType]):
    """Protocol for agents that can be passed to `loop()`.

    This encapsulates the parts of the :doc:`Stable Baselines API
    <sb3:modules/base>` that are relevant for this module.
    """

    def predict(
        self, obs: ObsType_contra, state: StateType | None = None
    ) -> tuple[ActType_co, StateType]:
        """Given an observation, predict the next optimal action to take.

        The *state* parameter may be used for recurrent agents. On the
        first invocation, None is passed. Afterwards, the state returned
        by the previous call is passed back in.
        """


class _FuncAgent(Agent[ObsType_contra, ActType_co, None]):
    def __init__(self, func: t.Callable[[ObsType_contra], ActType_co]) -> None:
        self._func = func

    @override
    def predict(
        self, obs: ObsType_contra, state: None = None
    ) -> tuple[ActType_co, None]:
        return self._func(obs), None


def _normalize_env(
    agent: t.Any, env: Env[ObsType_contra, ActType_co] | None
) -> Env[ObsType_contra, ActType_co]:
    """Return either `env` or `agent.env`."""
    if env is None:
        env = getattr(agent, "env", None)
    if env is None:
        raise ValueError("no environment passed")
    return env
