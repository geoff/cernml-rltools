# SPDX-FileCopyrightText: 2020 - 2024 CERN
# SPDX-FileCopyrightText: 2023 - 2024 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

"""Provide `BypassCache`, a helper for use with `functools.cache`."""

from __future__ import annotations

import typing as t

if t.TYPE_CHECKING:
    from typing_extensions import Self

__all__ = ("BypassCache",)


T = t.TypeVar("T")


@t.final
class BypassCache(tuple[T]):
    """Tell `functools.cache` to ignore this argument.

    This works by making all objects of this type compare and hash
    equal, no matter their contained value.

    Example:
        >>> from functools import cache
        ...
        >>> @cache
        ... def compute(x: str, y: BypassCache[str]) -> str:
        ...     [y] = y
        ...     print('computing', x, 'using', y)
        ...     return x
        ...
        >>> compute('result', BypassCache('logger'))
        computing result using logger
        'result'
        >>> compute('result', BypassCache('logger'))
        'result'
        >>> compute('result', BypassCache('different logger'))
        'result'

    Arguments are not wrapped automatically. To unwrap them, you can
    either use tuple unpacking or the `value` property:

        >>> x = BypassCache('x')
        >>> x
        BypassCache('x')
        >>> x.value
        'x'
        >>> [v] = x
        >>> v
        'x'

    Note that wrappers compare equal even if they are annotated as
    different generic aliases, eg:

        >>> x: BypassCache[str] = BypassCache('x')
        >>> y: BypassCache[int] = BypassCache(3)
        >>> x == y
        True
        >>> hash(x) == hash(y)
        True

    Wrappers are implemented as one-element tuples. However, they will
    not compare equal to them:

        >>> x[0] = 'y'
        Traceback (most recent call last):
        ...
        TypeError: 'BypassCache' object does not support item assignment
        >>> len(x)
        1
        >>> x == ('x',)
        False
    """

    __slots__ = ()

    def __new__(cls, value: T, /) -> Self:
        return super().__new__(cls, (value,))

    @property
    def value(self) -> T:
        """The wrapped value."""
        return self[0]

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self[0]!r})"

    __str__ = __repr__

    def __eq__(self, other: object) -> bool:
        return type(self) is type(other)

    def __hash__(self) -> int:
        return hash(type(self))
