# SPDX-FileCopyrightText: 2020 - 2024 CERN
# SPDX-FileCopyrightText: 2023 - 2024 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

"""Env wrappers in addition to those of :doc:`Gymnasium <gym:api/wrappers>`."""

from __future__ import annotations

import enum
import time
import typing as t

import numpy as np
from gymnasium import ActionWrapper, Env, ObservationWrapper, Wrapper, spaces
from gymnasium.core import ActType, ObsType
from typing_extensions import TypeGuard, override

from cernml import gym_utils, mpl_utils

from ._strenum import StrEnum
from .renderers import FigureRenderer, RewardsLogRenderer

if t.TYPE_CHECKING:
    from numpy.typing import NDArray

    from cernml.coi import GoalEnv, GoalObs, InfoDict

__all__ = (
    "wrap_with_chain",
    "AtEndOfEpisode",
    "DontRender",
    "EndOnOutOfRangeReward",
    "ExtractObservation",
    "LogRewards",
    "PenalizeFailure",
    "RenderOnStep",
    "RenderWrapper",
    "RescaleObservation",
    "RewardSuccess",
    "ScaleAction",
    "StandardizeObservation",
)


DType = t.TypeVar("DType", bound=np.generic)


if not t.TYPE_CHECKING:
    GoalType = t.TypeVar("GoalType")
    GoalType.__qualname__ = "cernml.coi.GoalType"
    GoalType.__module__ = "cernml.coi"

    class GoalObs(t.Generic[ObsType, GoalType]):
        """Dummy to make autodoc parse `ExtractObservation`'s bases correctly."""

        __qualname__ = "cernml.coi.GoalObs"
        __module__ = "cernml.coi"
        __slots__ = ()

    class NDArray(t.Generic[DType]):
        """Dummy class to force autodoc to parse `RecordSteps`'s bases correctly."""

        __qualname__ = "numpy.typing.NDArray"
        __module__ = "numpy.typing"
        __slots__ = ()


def wrap_with_chain(env: Env, chain: t.Iterable[t.Callable[[Env], Env] | tuple]) -> Env:
    """Alternative syntax for declaring a chain of Env wrappers.

    Args:
        env: The environment to be wrapped.
        chain: An iterable of wrapper-likes.

    Wrappers are applied to *env* successively, so that the last one in
    *chain* is the outermost. Each item of *chain* is either:

    - a bare class :samp:`{wrapper}` (called as
      :samp:`{wrapper}({inner})`), or
    - a tuple :samp:`({wrapper}, *{args})` (called as
      :samp:`{wrapper}({inner}, *{args})`).

    Example:
        >>> from functools import partial
        ...
        >>> class EmptyEnv(Env):
        ...     observation_space = spaces.Box(-1, 1, (1,))
        ...     action_space = spaces.Box(-1, 1, (1,))
        ...
        >>> env = wrap_with_chain(EmptyEnv(), [
        ...     LogRewards,
        ...     (ScaleAction, 0.5),
        ...     partial(RenderOnStep, timeout=0.5),
        ... ])
        >>> assert isinstance(env, RenderOnStep)
        >>> assert isinstance(env.env, ScaleAction)
        >>> assert isinstance(env.env.env, LogRewards)
        >>> assert isinstance(env.env.env.env, EmptyEnv)
        >>> assert isinstance(env.unwrapped, EmptyEnv)
    """
    for entry in chain:
        class_: t.Callable[..., Env]
        class_, *args = entry if isinstance(entry, tuple) else (entry,)
        env = class_(env, *args)
    return env


class EndOnOutOfRangeReward(Wrapper[ObsType, ActType, ObsType, ActType]):
    """Wrapper that ends an episode if a reward leaves a certain range.

    Args:
        env: The environment to wrap.
        lower: If the reward is below this threshold, the episode is
            truncated.
        upper: If the reward is above this threshold, the espiode is
            terminated.

    If either threshold is not passed, the wrapper attempts to access
    :samp:`{env}.get_wrapper_attr('reward_range')` and use its result.

    To disable a limit, set it to positive or negative infinity, or NaN.
    """

    _reward_range: tuple[t.SupportsFloat, t.SupportsFloat]

    def __init__(
        self,
        env: Env[ObsType, ActType],
        lower: float | None = None,
        upper: float | None = None,
    ) -> None:
        super().__init__(env)
        if lower is None or upper is None:
            env_lower, env_upper = env.get_wrapper_attr("reward_range")
            if lower is None:
                lower = env_lower
            if upper is None:
                upper = env_upper
        self.reward_range = lower, upper

    @property  # type: ignore[override]
    def reward_range(self) -> tuple[t.SupportsFloat, t.SupportsFloat]:
        """The range deduced from the constructor args.

        Either value may be infinity to signify no limit in that
        direction.
        """
        return self._reward_range

    @reward_range.setter
    def reward_range(self, value: tuple[t.SupportsFloat, t.SupportsFloat]) -> None:
        self._reward_range = value

    @override
    def step(
        self, action: ActType
    ) -> tuple[ObsType, t.SupportsFloat, bool, bool, InfoDict]:
        lower, upper = self.get_wrapper_attr("reward_range")
        obs, reward, terminated, truncated, info = super().step(action)
        freward = float(reward)
        if freward >= upper:
            terminated = True
        elif freward <= lower:
            truncated = True
        return obs, reward, terminated, truncated, info


class AtEndOfEpisode(Wrapper[ObsType, ActType, ObsType, ActType]):
    """Wrapper that calls back at the end of each episode.

    This is for simple hooks into the training or eval loop. For more
    sophisticated transformations, a more specialized wrapper should be
    used.

    Args:
        env: The environment to wrap.
        callback: A function that is called as :samp:`callback({info})`
            whenever an episode ends.
    """

    def __init__(
        self, env: Env[ObsType, ActType], callback: t.Callable[[InfoDict], t.Any]
    ) -> None:
        super().__init__(env)
        self.callback = callback

    @override
    def step(
        self, action: ActType
    ) -> tuple[ObsType, t.SupportsFloat, bool, bool, InfoDict]:
        obs, reward, terminated, truncated, info = super().step(action)
        if terminated or truncated:
            self.callback(info)
        return obs, reward, terminated, truncated, info


class ExtractObservation(ObservationWrapper[ObsType, ActType, GoalObs[ObsType, t.Any]]):
    """Wrapper that simplifies the observations of GoalEnv.

    This is meant to wrap a `~cernml.coi.GoalEnv`. `~cernml.coi.GoalEnv`
    objects are expected to return a `dict <.GoalObs>` with keys
    ``'observation'``, ``'desired_goal'`` and ``'achieved_goal'``. This
    wrapper extracts the ``'observation'`` value and discards the
    others.

    Args:
        env: The environment to wrap.
    """

    def __init__(
        self,
        env: GoalEnv[ObsType, t.Any, ActType] | Env[GoalObs[ObsType, t.Any], ActType],
    ) -> None:
        super().__init__(env)
        ob_space = env.observation_space
        assert isinstance(ob_space, spaces.Dict), ob_space
        self.observation_space = ob_space["observation"]

    @override
    def observation(self, observation: GoalObs[ObsType, t.Any]) -> ObsType:
        return observation["observation"]


class ScaleAction(ActionWrapper[ObsType, NDArray[DType], NDArray[DType]]):
    """Gym wrapper that scales the action up or down.

    This applies a simple scale on the action without modifying the
    action space. You can use this to either extend or reduce the step
    size that RL algorithms take.

    Args:
        env: The environment to wrap.
        factor: The factor by which to multiply each action before
            forwarding it to *env*.
    """

    def __init__(
        self, env: Env[ObsType, NDArray[DType]], factor: float | NDArray[DType]
    ) -> None:
        super().__init__(env)
        self.factor = np.asarray(factor, dtype=env.action_space.dtype)

    @override
    def action(self, action: NDArray[DType]) -> NDArray[DType]:
        return action * self.factor


class RescaleObservation(ObservationWrapper[NDArray[DType], ActType, NDArray[DType]]):
    """Rescales the continuous observation space to a range [a,b].

    This makes use of the observation space to perform the linear
    transformation.
    """

    observation_space: spaces.Box

    def __init__(self, env: Env[NDArray[DType], ActType], a: float, b: float) -> None:
        super().__init__(env)
        ob_space = env.observation_space
        if not isinstance(ob_space, spaces.Box):
            raise TypeError(f"expected Box action space, got {type(ob_space)}")
        self.observation_space = spaces.Box(
            low=a,
            high=b,
            shape=ob_space.shape,
            dtype=t.cast("type[np.floating | np.integer]", ob_space.dtype),
        )

    @override
    def observation(self, observation: NDArray[DType]) -> NDArray[DType]:
        ob_space = self.env.observation_space
        assert isinstance(ob_space, spaces.Box), ob_space
        scaled = gym_utils.scale_from_box(ob_space, observation)
        return gym_utils.unscale_into_box(self.observation_space, scaled)


class StandardizeObservation(
    ObservationWrapper[NDArray[np.double], ActType, NDArray[np.number]]
):
    """Wrapper that standardizes the observations of an environment.

    Standardized observations approximately have a mean of zero and a
    standard deviation of one. The necessary transformation is
    determined by sampling :samp:`{env}.reset()` a number of times.

    The statistical moments of non-initial observations may differ.

    Args:
        env: The environment to wrap.
        precision: Call :samp:`{env}.reset()` this many times to
            calculate the statistical moments.

    Raises:
        TypeError: if *env* doesn't have a `~gymnasium.spaces.Box`
            observation space.
    """

    observation_space: spaces.Box

    def __init__(
        self, env: Env[NDArray[np.number], ActType], precision: int = 10
    ) -> None:
        super().__init__(env)
        data = np.stack([env.reset()[0] for _ in range(precision)])
        self.mean = np.mean(data, dtype=np.double, axis=0)
        self.std = np.std(data, dtype=np.double, ddof=1, axis=0)
        # Avoid division by zero in `observation()`.
        self.std[self.std == 0.0] = 1.0
        ob_space = self.env.observation_space
        if not isinstance(ob_space, spaces.Box):
            raise TypeError(f"observation space must be Box: {type(ob_space)}")
        self.observation_space = spaces.Box(
            low=(ob_space.low - self.mean) / self.std,
            high=(ob_space.high - self.mean) / self.std,
            dtype=np.double,
        )

    @override
    def observation(self, observation: NDArray[np.number]) -> NDArray[np.double]:
        return (observation - self.mean) / self.std


class DontRender(Wrapper[ObsType, ActType, ObsType, ActType]):
    """Wrapper whose `render() <cernml.coi.Problem.render>` method does nothing.

    Most `RenderWrapper` wrappers render themselves and the wrapped
    environment whenever requested. This wrappers does not do that. You
    can use it when you're only interested in the output of the
    wrappers, but not the environment itself.
    """

    @override
    def render(self) -> None:
        return


class RenderOnStep(Wrapper[ObsType, ActType, ObsType, ActType]):
    """Wrapper that calls `render() <cernml.coi.Problem.render>` on every step.

    This is mostly for debugging, as you can see the evolution of an
    environment in real time.

    Args:
        env: The environment to wrap.
        timeout: If passed and not None, wait for this many seconds
            after each render. In render mode :rmode:`"human"`, this
            uses :func:`matplotlib.pyplot.pause()` in order to let the event loop
            run; otherwise, it uses :func:`time.sleep()`.
        enabled: Enables or disables this wrapper, depending on the
            value and the `~cernml.coi.Problem.render_mode` of *env*.
            For backwards compatibility, you may also pass True
            (corresponds to `~EnabledMode.ALWAYS`) and False
            (corresponds to `~EnabledMode.NEVER`).

    Attributes:
        enabled (EnabledMode): Determines whether to call render on each
            step. See the enum for the meaning of each value.

    """

    @enum.unique
    class EnabledMode(StrEnum):
        """The type of `.enabled`."""

        @override
        @classmethod
        def _missing_(cls, value: object) -> t.Any:
            if value is True:
                return cls.ALWAYS
            if value is False:
                return cls.NEVER
            return super()._missing_(value)

        ALWAYS = enum.auto()
        """Render in all render modes except :rmode:`None`."""
        HUMAN = enum.auto()
        """Only render in render mode :rmode:`"human"`. With this value,
        you satisfy the requirement of :rmode:`"human"` that `render()
        <cernml.coi.Problem.render>` be called automatically."""
        NEVER = enum.auto()
        """Don't render. This effectively disables the wrapper."""

    def __init__(
        self,
        env: Env[ObsType, ActType],
        timeout: float | None = None,
        enabled: EnabledMode | str = "always",
    ) -> None:
        super().__init__(env)
        self._timeout = timeout
        self.enabled = self.EnabledMode(enabled)

    @override
    def reset(
        self, *, seed: int | None = None, options: InfoDict | None = None
    ) -> tuple[ObsType, InfoDict]:
        res, info = super().reset(seed=seed, options=options)
        if self._is_enabled():
            self.render()
            self._sleep()
        return res, info

    @override
    def step(
        self, action: ActType
    ) -> tuple[ObsType, t.SupportsFloat, bool, bool, InfoDict]:
        res = super().step(action)
        if self._is_enabled():
            self.render()
            self._sleep()
        return res

    def _is_enabled(self) -> bool:
        render_mode = self.render_mode
        if render_mode is None:
            return False
        if render_mode == "human":
            return self.enabled != self.EnabledMode.NEVER
        return self.enabled == self.EnabledMode.ALWAYS

    def _sleep(self) -> None:
        if self._timeout:
            import matplotlib.pyplot as plt

            sleep = plt.pause if self.render_mode == "human" else time.sleep
            sleep(self._timeout)


class RewardSuccess(Wrapper[ObsType, ActType, ObsType, ActType]):
    """Wrapper that gives a bonus if the episode ends successfully.

    Args:
        env: The environment to wrap.
        bonus: The amount by which to increase the reward if the episode
            has been terminated.

    This wrapper adds the following key to :ref:`the info dict
    <guide/control_flow:the info dict>`:

    .. infodictkey:: "RewardSuccess.bonus"
        :type: float

        Set if the episode has ended. The value is *bonus* if the
        episode ended successfully, zero otherwise.
    """

    def __init__(self, env: Env[ObsType, ActType], bonus: float) -> None:
        super().__init__(env)
        self.bonus = bonus

    @override
    def step(
        self, action: ActType
    ) -> tuple[ObsType, t.SupportsFloat, bool, bool, InfoDict]:
        obs, reward, terminated, truncated, info = super().step(action)
        if terminated or truncated:
            bonus = self.bonus * terminated
            info["RewardSuccess.bonus"] = bonus
            reward = float(reward) + bonus
        return obs, reward, terminated, truncated, info


class PenalizeFailure(Wrapper[ObsType, ActType, ObsType, ActType]):
    """Wrapper that reduces the reward by a penalty when diverging.

    Args:
        env: The environment to wrap.
        penalty: The amount by which to reduce the reward if the episode
            has been truncated, but not terminated.


    This wrapper adds the following key to :ref:`the info dict
    <guide/control_flow:the info dict>`:

    .. infodictkey:: "PenalizeFailure.penalty"
        :type: float

        Set if the episode has ended. The value is zero if the episode
        ended successfully, *penalty* otherwise.
    """

    def __init__(self, env: Env, penalty: float) -> None:
        super().__init__(env)
        self.penalty = penalty

    @override
    def step(
        self, action: ActType
    ) -> tuple[ObsType, t.SupportsFloat, bool, bool, InfoDict]:
        obs, reward, terminated, truncated, info = super().step(action)
        if terminated or truncated:
            penalty = self.penalty if truncated and not terminated else 0.0
            info["PenalizeFailure.penalty"] = penalty
            reward = float(reward) - penalty
        return obs, reward, terminated, truncated, info


class RenderWrapper(Wrapper[ObsType, ActType, ObsType, ActType]):
    """Base class for wrappers that have a figure they can render to.

    This wraps some common logic and ensures each wrapper provides the
    same interface.

    On rendering, this class does not actually enter the GUI loop, so
    the rendered results will likely not be visible. If you want to see
    the results while in a hot loop, consider wrapping these wrappers in
    `RenderOnStep`, which regularly gives the GUI loop a chance to
    handle events.

    Args:
        env: The environment to wrap.
        renderer: The renderer used by the subclass. This actually
            implements the render logic.

    Note:
        Don't forget to `close() <cernml.coi.Problem.close>` the wrapped
        environment after you're done. This frees any resources
        associated with the wrapped renderer.
    """

    renderer: FigureRenderer
    """The renderer used by this instance."""

    def __init__(self, env: Env[ObsType, ActType], renderer: FigureRenderer) -> None:
        super().__init__(env)
        self.renderer = renderer

    @override
    def render(self, **kwargs: t.Any) -> t.Any:
        result: mpl_utils.MatplotlibFigures | None = super().render(**kwargs)
        if self.render_mode == "human":
            self.renderer.update()
        if self.render_mode == "matplotlib_figures":
            extra = self.renderer.update()
            assert result is not None
            assert extra is not None
            result = mpl_utils.concat_matplotlib_figures(result, extra)
        return result

    @override
    def close(self) -> None:
        self.renderer.close()
        return super().close()


class LogRewards(RenderWrapper[ObsType, ActType]):
    """Gym wrapper that logs the outcome of each finished episode.

    Args:
        env: The environment to wrap.
        info_initial: Callback to determine the :term:`initial reward`
            from the info dict.
        obs_initial: Callback to determine the :term:`initial reward`
            from the initial observation.
        info_objective: Callback to determine the :term:`reward
            objective` from the info dict.
        env_objective: Callback to determine the :term:`reward
            objective` from *env*.

    Raises:
        TypeError: If two conflicting extraction callbacks are passed,
            e.g. both *info_initial* and *obs_initial*, or both
            *info_objective* and *env_objective*.

    Rendering this wrapper produces a figure with two subplots. The
    first one shows for each episode its length and whether it was
    a *success*. The second one shows the final reward of each episode
    and, if possible, the *initial reward* and the *reward objective*.

    .. glossary::

        initial reward
            This is a measure of the "quality" of the observation
            returned by `reset() <gymnasium.Env.reset>`. This only works
            if the reward returned by `step() <gymnasium.Env.step>`
            depends only on the state reached and not on the action it
            took to get there.

            The initial reward is determined from :samp:`({obs}, {info})
            = {env}.reset()`. The process works as follows:

            - if *info_initial* is passed, it is called like
              :samp:`info_initial({info})`;

            - if *obs_initial* is passed, it is called like
              :samp:`obs_initial({obs})`;

            - if neither is passed, a **default implementation** is
              used. It proceeds as follows:

              1. it looks for a method :samp:`{env}.compute_reward()`
                 and calls it with arguments based on *obs* and *info*
                 (see the note below);
              2. failing that, it uses the key :idkey:`"reward"` of
                 *info*;
              3. failing that, it returns `~math.nan`.

            .. note::
                The method ``compute_reward()`` is found via
                `get_wrapper_attr()
                <cernml.coi.Problem.get_wrapper_attr>`. It is defined by
                both `~cernml.coi.SeparableEnv` and `GoalEnv`.
                To determine which arguments to pass, the above default
                implementation inspects `observation_space` at the time
                of instantiation.

        reward objective
            This is a threshold on the reward returned by `step()
            <gymnasium.Env.step>`. If it is reached at the end of the
            episode, the wrapper assumes that the was a success.

            The reward objective is determined from :samp:`(_obs,
            _reward, _term, _trunc, {info}) = {env}.step({action})`. The
            process works as follows:

            1. if *info_objective* is passed, it is called like
               :samp:`info_objective({info})`;

            2. if *env_objective* is passed, it is called like
               :samp:`env_objective({env})`;

            3. if neither is passed, a **default implementation** is
               used. It attempts to use the info dict keys
               :idkey:`"objective"` and :idkey:`"reward_limits"`, in
               this order. If both are absent, it returns `~math.inf`.

        success
            An episode may end in a success or a failure or in an
            ambiguous state. This information is read directly from the
            :idkey:`"success"` key of the info dict returned by `step()
            <gymnasium.Env.step>`.

    This wrapper reads the following keys from the :ref:`info dict
    <guide/control_flow:the info dict>`:

    .. infodictkey:: "objective"
        :type: float

        `step() <gymnasium.Env.step>` may return this to
        indicate the step reward value above which an episode
        can be counted as a success.

    .. infodictkey:: "reward_limits"
        :type: tuple[float, float]

        `step() <gymnasium.Env.step>` may return this to
        indicate the step reward values above and below which
        the episode is counted as a success and a failure
        respectively.
    """

    renderer: RewardsLogRenderer
    """The renderer constructed by this instance."""

    def __init__(
        self,
        env: Env[ObsType, ActType],
        *,
        info_initial: t.Callable[[InfoDict], float] | None = None,
        obs_initial: t.Callable[[ObsType], float] | None = None,
        info_objective: t.Callable[[InfoDict], float] | None = None,
        env_objective: t.Callable[[Env[ObsType, ActType]], float] | None = None,
    ) -> None:
        super().__init__(
            env, RewardsLogRenderer("Episodes history", render_mode=env.render_mode)
        )
        self._get_objective = _guess_objective(info_objective, env_objective)
        self._get_initial = _guess_initial(info_initial, obs_initial, env)

    @override
    def reset(self, **kwargs: t.Any) -> tuple[ObsType, InfoDict]:
        obs, info = super().reset(**kwargs)
        reward = self._get_initial(obs, info)
        self.renderer.start_next_episode(reward)
        return obs, info

    @override
    def step(
        self, action: ActType
    ) -> tuple[ObsType, t.SupportsFloat, bool, bool, InfoDict]:
        obs, reward, terminated, truncated, info = super().step(action)
        self.renderer.update_current_episode(
            reward=float(reward),
            success=info.get("success"),
            threshold=self._get_objective(info, self.env),
        )
        return obs, reward, terminated, truncated, info

    def end_training(self) -> None:
        """Mark the end of training.

        After this is called, the figure used by `renderer` will color
        all episodes up to the current one in a different color.
        """
        self.renderer.end_training()


@t.overload
def _guess_initial(
    info_initial: t.Callable[[InfoDict], float] | None,
    obs_initial: t.Callable[[ObsType], float] | None,
    env: GoalEnv[ObsType, t.Any, t.Any],
) -> t.Callable[[GoalObs[ObsType, t.Any], InfoDict], float]: ...
@t.overload
def _guess_initial(
    info_initial: t.Callable[[InfoDict], float] | None,
    obs_initial: t.Callable[[ObsType], float] | None,
    env: Env[ObsType, t.Any],
) -> t.Callable[[ObsType, InfoDict], float]: ...
def _guess_initial(
    info_initial: t.Callable[[InfoDict], float] | None,
    obs_initial: t.Callable[[ObsType], float] | None,
    env: Env,
) -> t.Callable[[t.Any, InfoDict], float]:
    """Guess the callback that finds the reward for an observation."""
    if info_initial is not None and obs_initial is not None:
        raise TypeError("must not pass info_initial and obs_initial")
    if info_initial is not None:
        return lambda _, info: info_initial(info)
    if obs_initial is not None:
        return lambda obs, _: obs_initial(obs)

    try:
        compute_reward = env.get_wrapper_attr("compute_reward")
    except AttributeError:
        return lambda _, info: info.get("reward", np.nan)

    from_env: t.Callable[[t.Any, InfoDict], float]
    if _is_goal_env(env):

        def from_env(obs: GoalObs[ObsType, t.Any], info: InfoDict) -> float:
            return compute_reward(obs["achieved_goal"], obs["desired_goal"], info)

    else:

        def from_env(obs: ObsType, info: InfoDict) -> float:
            return compute_reward(obs, None, info)

    return from_env


def _is_goal_env(env: Env) -> TypeGuard[GoalEnv]:
    ob_space = env.observation_space
    keys = {"observation", "achieved_goal", "desired_goal"}
    return isinstance(ob_space, spaces.Dict) and keys.issubset(ob_space.keys())


def _guess_objective(
    info_objective: t.Callable[[InfoDict], float] | None,
    env_objective: t.Callable[[Env[ObsType, ActType]], float] | None,
) -> t.Callable[[InfoDict, Env], float]:
    """Guess the callback that finds the reward objective."""
    if info_objective is not None and env_objective is not None:
        raise TypeError("must not pass info_objective and env_objective")
    if info_objective is not None:
        return lambda info, _: info_objective(info)
    if env_objective is not None:
        return lambda _, env: env_objective(env)

    def _default(info: dict[str, t.Any], _: t.Any) -> float:
        objective = info.get("objective")
        if objective is None:
            objective = info.get("reward_limits", (-np.inf, np.inf))[1]
        return objective

    return _default


if not t.TYPE_CHECKING:
    del GoalType, GoalObs, NDArray
