# SPDX-FileCopyrightText: 2020 - 2024 CERN
# SPDX-FileCopyrightText: 2023 - 2024 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

"""This package optionally supports `click` in addition to `argparse`.

To avoid import errors, the Click support is encapsulated in a dedicated
module `.click_plugin`, which must be imported separately. Once you have
imported it, you can use the `@cache_control_options()
<cache_control_options>` decorator as a replacement for
`.add_cache_control_group()`, and the `.TrainingCacher` class instead
of `.train_or_load_module()`.

Example:
    >>> from cernml.rltools.clicache import (
    ...     TrainingCacher,
    ...     click_plugin,
    ... )
    ...
    >>> def train(module, *args, **kwargs):
    ...     ...
    ...
    >>> @click.command()
    ... @click_plugin.cache_control_options()
    ... def main(cache: Path | None, train: clicache.TrainWhen) -> None:
    ...     cacher = TrainingCacher(cache, train)
    ...     module = ...
    ...     history = cacher(train, module, ...)
    ...     ...
    ...
    >>> if __name__ == "__main__":
    ...     main()
"""

from __future__ import annotations

import functools
import pathlib
import typing as t

import click
from typing_extensions import override

from .. import clicache

FC = t.TypeVar("FC", bound=t.Union[t.Callable, click.Command])


def cache_control_options() -> t.Callable[[FC], FC]:
    """Add cache control arguments to your CLI.

    This simply combines `cache()` and `train_when()` into a single
    decorator.
    """
    T = t.TypeVar("T")

    def compose(*fns: t.Callable[[T], T]) -> t.Callable[[T], T]:
        def wrapper(x: T) -> T:
            return functools.reduce(lambda x, f: f(x), fns, x)

        return wrapper

    return compose(train_when(), cache())


def cache() -> t.Callable[[FC], FC]:
    """Wrapper around :func:`click.option()` for :option:`--cache`."""
    return click.option(
        "--cache",
        type=CacheFile(),
        metavar="CACHE",
        help="cache and re-use the result of training in the given file",
    )


def train_when() -> t.Callable[[FC], FC]:
    """Wrapper around :func:`click.option()` for :option:`--train`."""
    return click.option(
        "-t",
        "--train",
        type=TrainWhen(),
        default="auto",
        metavar="WHEN",
        help="determines when to retrain a model.\n"
        "\n"
        "\b\n"
        "always: always train from scratch;\n"
        "auto: load training results from --cache if possible,\n"
        "      otherwise train from scratch;\n"
        "never: refuse to train if --cache cannot be used",
    )


class CacheFile(click.Path):
    """Parameter type for :option:`--cache`.

    This pre-determines some parameters of `click.Path` while leaving
    others for you to override.

    Args:
        file_okay: Allow a file as a value.
        dir_okay: Allow a directory as a value.
        readable: if true, a readable check is performed.
        writable: if true, a writable check is performed.
        executable: if true, an executable check is performed.
        resolve_path: Make the value absolute and resolve any symlinks.
            A ``~`` is not expanded, as this is supposed to be done by
            the shell only.
        allow_dash: Allow a single dash as a value, which indicates
            a standard stream (but does not open it). Use
            :func:`click.open_file()` to handle opening this value.
    """

    def __init__(
        self,
        file_okay: bool = True,
        dir_okay: bool = True,
        resolve_path: bool = False,
        allow_dash: bool = False,
    ) -> None:
        super().__init__(
            file_okay=file_okay,
            dir_okay=dir_okay,
            resolve_path=resolve_path,
            allow_dash=allow_dash,
            exists=False,
            writable=True,
            readable=True,
            executable=False,
            path_type=pathlib.Path,
        )
        if self.file_okay and not self.dir_okay:
            self.name: str = "cache file"
        elif self.dir_okay and not self.file_okay:
            self.name = "cache directory"
        else:
            self.name = "cache path"


class TrainWhen(click.Choice):
    """Parameter type for :option:`--train`.

    This pre-determines some parameters of `click.Choice` in a way that
    is reasonable for this option.
    """

    name = "train mode"

    def __init__(self) -> None:
        super().__init__(choices=[mode.value for mode in clicache.TrainWhen])

    @override
    def convert(
        self, value: t.Any, param: click.Parameter | None, ctx: click.Context | None
    ) -> clicache.TrainWhen:
        value = super().convert(value, param, ctx)
        return clicache.TrainWhen(value)

    def __repr__(self) -> str:
        cls = type(self)
        return f"{cls.__module__}.{cls.__qualname__}()"
