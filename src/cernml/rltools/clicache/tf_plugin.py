# SPDX-FileCopyrightText: 2020 - 2024 CERN
# SPDX-FileCopyrightText: 2023 - 2024 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

"""This built-in plugin allows caching of Tensorflow models."""

from __future__ import annotations

import functools
import typing as t

from typing_extensions import override

from . import CacheHandler

if t.TYPE_CHECKING:
    from pathlib import Path

    import tensorflow as tf

__all__ = (
    "TensorflowCacheHandler",
    "tf_plugin",
)


def tf_plugin(module: object, /) -> CacheHandler | None:
    """The `.CachePlugin` for Tensorflow modules. Available by default.

    This checks first that *module* inherits from a class in the
    :mod:`tensorflow` namespace. It does so by checking the
    `~function.__module__` attribute of all base classes. If a match is
    found, :func:`isinstance()` is used to verify that *module* is
    a `tf.Module`.

    This ensures that the slow import of Tensorflow only happens at
    a point where it likely is already loaded.

    The handler returned by this function caches *module* via
    :meth:`tf.train.Checkpoint.write()` and restores it via
    :meth:`tf.train.Checkpoint.restore()`.
    """
    if not _name_match(module):
        return None
    Module = _lazy_load_module()
    if not isinstance(module, Module):
        return None
    return TensorflowCacheHandler(module)


class TensorflowCacheHandler(CacheHandler):
    """Cache that saves TensorFlow models."""

    def __init__(self, model: tf.Module) -> None:
        Checkpoint = _lazy_load_checkpoint()
        self._checkpoint = Checkpoint(model=model)

    @override
    def save(self, path: Path) -> None:
        """Save *module* via :meth:`tensorflow.train.Checkpoint.write()`."""
        self._checkpoint.write(str(path))

    @override
    def load(self, path: Path) -> None:
        """Restore via :meth:`tensorflow.train.Checkpoint.restore()`."""
        self._checkpoint.restore(str(path)).assert_consumed()


def _name_match(module: object) -> bool:
    return any(
        getattr(cls, "__module__", "").startswith("tensorflow.")
        for cls in module.__class__.__mro__
    )


@functools.cache
def _lazy_load_module() -> type[tf.Module]:
    from tensorflow import Module

    return Module


@functools.cache
def _lazy_load_checkpoint() -> type[tf.train.Checkpoint]:
    from tensorflow.train import Checkpoint

    return Checkpoint
