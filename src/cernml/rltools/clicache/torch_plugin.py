# SPDX-FileCopyrightText: 2020 - 2024 CERN
# SPDX-FileCopyrightText: 2023 - 2024 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

"""This built-in plugin allows caching of PyTorch modules."""

from __future__ import annotations

import functools
import typing as t
from pathlib import Path

from typing_extensions import override

from . import CacheHandler

if t.TYPE_CHECKING:
    import torch

__all__ = (
    "TorchCacheHandler",
    "torch_plugin",
)


def torch_plugin(module: object, /) -> CacheHandler | None:
    """The `.CachePlugin` for PyTorch modules. Available by default.

    This checks first that *module* inherits from a class in the `torch`
    namespace. It does so by checking the `~function.__module__` attribute
    of all base classes. If a match is found, :func:`isinstance()` is
    used to verify that *module* is a `torch.nn.Module`.

    This ensures that the slow import of PyTorch only happens at a point
    where it likely is already loaded.

    The handler returned by this function caches *module* via
    :func:`torch.save()` and restores it via :func:`torch.load()` and
    :meth:`~torch.nn.Module.load_state_dict()`.
    """
    if not _name_match(module):
        return None
    Module = _lazy_load_module()
    if not isinstance(module, Module):
        return None
    return TorchCacheHandler(module)


class TorchCacheHandler(CacheHandler):
    """Handler for PyTorch modules."""

    def __init__(self, module: torch.nn.Module) -> None:
        import torch

        self._torch = torch
        self._module = module

    @override
    def save(self, path: Path) -> None:
        """Save *module* via :func:`torch.save()`."""
        self._torch.save(self._module.state_dict(), path)

    @override
    def load(self, path: Path) -> None:
        """Restore *module* via :meth:`torch.nn.Module.load_state_dict()`."""
        self._module.load_state_dict(self._torch.load(path))


def _name_match(module: object) -> bool:
    return any(
        getattr(cls, "__module__", "").startswith("torch.")
        for cls in module.__class__.__mro__
    )


@functools.cache
def _lazy_load_module() -> type[torch.nn.Module]:
    from torch.nn import Module

    return Module
