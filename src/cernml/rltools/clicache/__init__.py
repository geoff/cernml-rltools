# SPDX-FileCopyrightText: 2020 - 2024 CERN
# SPDX-FileCopyrightText: 2023 - 2024 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

"""Facility to provide ML model caching to the command line.

This package provides functions to implement a uniform CLI for caching
a single ML model. It is straight-forward to use and out-of-the-box, it
supports `torch <.torch_plugin>`, `tensorflow <.tf_plugin>` and
`click <.click_plugin>`.

..
    >>> from unittest.mock import Mock
    >>> patch = getfixture('monkeypatch')
    >>> P = Mock(name='argparse.ArgumentParser')
    >>> P().parse_args().train = 'always'
    >>> patch.setattr('argparse.ArgumentParser', P)
    >>> del Mock, P, patch

..
    >>> from unittest.mock import Mock
    >>> torch = Mock(name='torch')
    >>> class Model(Mock):
    ...     __module__ = 'torch.nn'
    >>> torch.nn.Module = Model
    >>> import sys
    >>> patch = getfixture('monkeypatch')
    >>> patch.setitem(sys.modules, 'torch', torch)
    >>> patch.setitem(sys.modules, 'torch.nn', torch.nn)
    >>> del sys, Mock, patch, torch

    >>> import argparse
    >>> from cernml.rltools import clicache
    ...
    >>> parser = argparse.ArgumentParser()
    >>> clicache.add_cache_control_group(parser, "cache control")
    >>> args = parser.parse_args()
    ...
    >>> def train(model):
    ...     history = model.train(...)
    ...     return history
    ...
    >>> model = Model()
    >>> history = clicache.train_or_load_module(args, train, model)
    >>> if history is None:
    ...     print("Model has been loaded from cache!")
    ... else:
    ...     print("Model has been trained!")
    Model has been trained!
"""

from __future__ import annotations

import abc
import argparse
import enum
import functools
import logging
import sys
import typing as t
from pathlib import Path

from typing_extensions import ParamSpec, deprecated

from .._bypass import BypassCache as Bypass
from .._strenum import StrEnum

if sys.version_info < (3, 10):
    import importlib_metadata as metadata
else:
    from importlib import metadata

if t.TYPE_CHECKING:
    from typing_extensions import Concatenate

__all__ = (
    "add_cache_control_group",
    "train_or_load_module",
    "TrainingCacher",
    "TrainWhen",
    "CacheHandler",
    "CacheError",
    "NoHandlerError",
    "NoCacheFileError",
    "SkipCache",
)


# --- Exception classes --- {{{1


class CacheError(Exception):
    """Base class of all exceptions of this package."""


@deprecated("return None instead of raising this exception")
class SkipCache(CacheError):
    """Signal that a cache plugin cannot handle the given object.

    .. deprecated:: 0.12

        Instead of raising this exception, return None from your
        `CachePlugin`.
    """


class NoHandlerError(CacheError):
    """No plugin can handle the given module."""


class NoCacheFileError(CacheError):
    """The given cache file doesn't exist."""


# --- Interface definition --- {{{1


CachePlugin: t.TypeAlias = "t.Callable[[object], CacheHandler | None]"
r"""alias of `Callable`\[[`object`], `CacheHandler` | `None`]

Entry points for this package should have this type. They should
accept an arbitrary object (the module that's being trained) and check
whether they can handle it. If yes, they should return a `CacheHandler`
that holds onto the given object, probably as an attribute. If not, they
should return `None` to let other plugins try.
"""


@t.runtime_checkable
class CacheHandler(t.Protocol):
    """Framework-neutral interface to save and load trained models.

    If you add your own `CachePlugin`, the return value should follow
    this protocol. Note that the module isn't passed again. Instead you
    should hold on to it, once your `CachePlugin` has accepted it.
    """

    @abc.abstractmethod
    def save(self, path: Path) -> None:
        """Write the attributes of *module* to a file.

        This should write the attributes of the *module* passed at
        initialization. You must be able to read them later via `load()`
        onto another existing object such that they are both identical
        for all purposes of evaluation. You need not save transient
        training data.

        The given *path* may already exist and may be a directory or
        a file. If it already exists, you should overwrite it.
        """

    @abc.abstractmethod
    def load(self, path: Path) -> None:
        """Read the attributes of *module* from a file.

        This is should in-place modify the *module* passed at
        initialization. You must not create a new object and reassign it
        to your attribute.

        If the cache file doesn't exist or cannot be used, you should
        raise an `OSError`. In this case, training proceeds as normal.
        Other exceptions are interpreted as unexpected and not caught.
        """


class NullHandler(CacheHandler):
    """Handler that doesn't do anything.

    This handler isn't used by default. You can append it to the
    `~TrainingCacher.plugins` list of a training cacher if you want it
    to silently disable caching in case no handler can be found.

    Because of the danger of data loss, this behavior is not enabled by
    default.
    """

    def save(self, path: Path) -> None:
        """Doesn't do anything."""

    def load(self, path: Path) -> None:
        """Always raises `FileNotFoundError`."""
        raise FileNotFoundError(path)


# --- TrainingCacher definition --- {{{1


@enum.unique
class TrainWhen(StrEnum):
    """Controls cache use when calling a `TrainingCacher`.

    These are also the possible values for the :option:`--train` option
    defined by `.add_cache_control_group()`. All functions that accept
    this enum also accept the corresponding string values.
    """

    AUTO = enum.auto()
    """Read the cache if it exists, otherwise train the module and save
    the result. (default)"""

    ALWAYS = enum.auto()
    """Don't read the cache, always train the module and save the
    result."""

    NEVER = enum.auto()
    """Read the cache if it exists, otherwise raise an exception."""

    @classmethod
    def _missing_(cls, value: object) -> TrainWhen | None:
        """When converting strings to this enum, case is ignored.

        >>> TrainWhen("auto") == TrainWhen("AUTO")
        True
        """
        if isinstance(value, str):
            return cls.__members__.get(str.upper(value))
        return None


M = t.TypeVar("M")
Ps = ParamSpec("Ps")
R = t.TypeVar("R")


class TrainingCacher:
    """More complex interface to `train_or_load_module()`.

    Args:
        path: The path from where to load the module, if possible or
            requested. Ignored if None.
        mode: Controls when to use the cache and when to train.
            Valid strings are `'auto' <TrainWhen.AUTO>`, `'always'
            <TrainWhen.ALWAYS>` and `'never' <TrainWhen.NEVER>`.
    """

    def __init__(
        self, cache: Path | None = None, train: TrainWhen | str = "auto"
    ) -> None:
        self.cache = cache
        self.train = TrainWhen(train)
        self.logger = logging.getLogger(__name__)

    def __call__(
        self,
        train_module: t.Callable[Concatenate[M, Ps], R],
        module: M,
        /,
        *args: Ps.args,
        **kwargs: Ps.kwargs,
    ) -> R | None:
        """Load the module or train and save it.

        Args:
            train_module: A callback that trains *module*. Called as
                :samp:`train_module({module}, *args, **kwargs)`. Training is
                expected to modify the module.
            module: Any object that can be trained via *train_module* and
                for which a `CacheHandler` exists.
            args, kwargs: Forwarded to *train_module*.

        Returns:
            If the module had to be trained, this returns the return
            value of *train_module*. If the cache has been used, this
            returns None.

        Raises:
            `NoHandlerError`: No `CacheHandler` can handle the given
                *module*.
            `NoCacheFileError`: The cache was unavailable and the user
                has requested to `never <TrainWhen.NEVER>` train.
            `OSError`: If saving a cache file fails for I/O-related
                reasons. When loading a cache file, this exceptions is
                logged, but otherwise ignored.
            `Exception`: Any exception raised withing *train_module*.
        """
        # If there is no handler, fail early. This prevents weird
        # situations where a script succeeds as long as you don't try to
        # use its caching feature.
        handler = self.find_handler(module)
        # No cache: Ignore entire logic and just train.
        if self.cache is None:
            self.logger.debug("no cache file given, training")
            return train_module(module, *args, **kwargs)
        # Cache exists: Load from it, if possible and requested.
        # If loading succeeds, we're done.
        if self.train != TrainWhen.ALWAYS:
            self.logger.debug("loading cache file: %s", self.cache)
            try:
                handler.load(self.cache)
            except OSError:
                self.logger.debug("cache file not found")
            else:
                return None
        # Loading has failed or was disabled. Train if training is
        # enabled. In any case, if training succeeds, we cache the
        # trained model.
        if self.train == TrainWhen.NEVER:
            raise NoCacheFileError("no cache file and training has been disabled")
        self.logger.debug("training module")
        result = train_module(module, *args, **kwargs)
        self.logger.debug("saving module to cache file: %s", self.cache)
        handler.save(self.cache)
        self.logger.debug("saved!")
        return result

    def find_handler(self, module: object) -> CacheHandler:
        """Find and instantiate a cache that can handle the module.

        Raises:
            `NoHandlerError`: if none of the plugins returned a handler.
        """
        logger = self.logger
        logger.debug("finding handler for module %r", module)
        for plugin in self.plugins:
            logger.debug("trying plugin %s", plugin)
            try:
                handler = plugin(module)
            except SkipCache:
                continue
            if handler is None:
                continue
            if not isinstance(handler, CacheHandler):
                logger.debug("plugin %s returned not a handler: %r", plugin, handler)
                continue
            logger.debug("using handler %r", handler)
            return handler
        msg = f"no cache handler found for {module!r}"
        raise NoHandlerError(msg)

    logger: logging.Logger
    """The logger used while trying cache handlers. This is the
    package-scope logger by default, but it may be replaced."""

    ep_group: str = "cernml.rltools.clicache"
    """The name of the entry point group from which the default cache
    plugins are loaded."""

    @functools.cached_property
    def plugins(self) -> list[CachePlugin]:
        """The list of cache plugins.

        This is a `~functools.cached_property`. It doesn't get
        initialized until the first access. It always gets pre-populated
        with the results of `load_entry_points()`, but you may modify it
        after the fact.
        """
        return list(load_entry_points(self.ep_group, logger=Bypass(self.logger)))


# --- Entry points --- {{{1


@functools.cache
def load_entry_points(
    group: str = "cernml.rltools.clicache",
    *,
    logger: Bypass[logging.Logger | None] = Bypass(None),  # noqa: B008
) -> t.Sequence[CachePlugin]:
    """Load all entry points of the given group.

    This function uses :func:`functools.cache()`, so you can call it as
    many times as you want and always get the same result.

    Entry points are ignored if loading them raises an exception or the
    loaded object is not callable. Such events are logged to the given
    *logger* at level `~logging.DEBUG`. If you don't pass a logger, the
    package-scope logger is used.
    """
    log = logger.value if logger.value is not None else logging.getLogger(__name__)
    del logger
    plugins: list[CachePlugin] = []
    log.debug("Loading entry points for group %s", group)
    for ep in metadata.entry_points(group=group):
        log.debug("Loading entry point %s", ep.name)
        try:
            plugin: CachePlugin | None = ep.load()
        except Exception:
            log.exception("failed to load entry point %s", ep.name)
            continue
        if not callable(plugin):
            log.error("loaded entry point %r is not callable: %r", ep.name, plugin)
            continue
        plugins.append(plugin)
    if log.isEnabledFor(logging.DEBUG):
        cache_names = [_get_full_name(plugin) for plugin in plugins]
        log.debug("default caches: %r", cache_names)
    return tuple(plugins)


def _get_full_name(cls: object) -> str:
    modname: str = getattr(cls, "__module__", "<no module>")
    clsname: str = getattr(cls, "__qualname__", "<no qualname>")
    return f"{modname!s}:{clsname!s}"


# --- argparse API --- {{{1


def add_cache_control_group(
    parser: argparse.ArgumentParser, *args: t.Any, **kwargs: t.Any
) -> None:
    """Add cache control arguments to your CLI.

    This function calls `parser.add_argument_group()
    <argparse.ArgumentParser.add_argument_group>` with all arguments
    after *parser*. Use it in conjunction with `train_or_load_module()`
    to provide a uniform CLI for caching trained modules.

    It defines the following two options:

    .. option:: --cache <FILE>

        Enable caching the result of training to the given *FILE*. If
        this option isn't passed, caching is skipped completely.

    .. option:: -t <WHEN>, --train <WHEN>

        Control when to use the cache. *WHEN* is one of the values of
        `TrainWhen`. Case is ignored.

    Examples:
        Train a module on the first call and save it in :file:`module.pt`.
        On subsequent calls, skip training and re-use results from file.

        .. code-block:: shell-session

            $ ./script.py --cache "module.pt"
            INFO: cache file: module.pt
            INFO: cache not found, training model
            INFO: done, saving: module.pt

            $ ./script.py --cache "module.pt"
            INFO: cache file: module.pt
            INFO: cache found, loading model from it

        Train a module and save it in :file:`module.pt`. Existing models
        are always module is overwritten.

        .. code-block:: shell-session

            $ ./script.py --cache "module.pt" --train="always"
            INFO: cache file: module.pt
            INFO: cache ignored, training model
            INFO: done, saving: module.pt

            $ ./script.py --cache "module.pt"
            INFO: cache file: module.pt
            INFO: cache ignored, training model
            INFO: done, saving: module.pt

        Reuse results from :file:`module.pt`. If the file doesn't exist,
        abort with an error.

        .. code-block:: shell-session

            $ ./script.py --cache "module.pt" --train="never"
            INFO: cache file: module.pt
            INFO: cache not found, no training allowed
            Traceback (most recent call last):
            ...
            NoWeights: no cache file and training has been disabled
    """
    cacher = parser.add_argument_group(*args, **kwargs)
    cacher.add_argument(
        "--cache",
        type=Path,
        metavar="CACHE",
        help="cache and re-use the result of training in the given file",
    )
    cacher.add_argument(
        "-t",
        "--train",
        choices=[when.value for when in TrainWhen],
        default=TrainWhen.AUTO,
        type=TrainWhen,
        metavar="WHEN",
        help='"always": always train from scratch; "auto": load training '
        "results from --cache if possible, otherwise train from scratch; "
        '"never": refuse to train if --cache cannot be used',
    )


def train_or_load_module(
    namespace: argparse.Namespace,
    train_module: t.Callable[Concatenate[M, Ps], R],
    module: M,
    /,
    *args: Ps.args,
    **kwargs: Ps.kwargs,
) -> R | None:
    """Handle arguments defined by `add_cache_control_group()`.

    Args:
        namespace: The return value of `parser.parse_args()
            <argparse.ArgumentParser.parse_args>` after
            `add_cache_control_group()` was called on the parser.
        train_module: A callback that trains *module*. Called as
            :samp:`train_module({module}, *args, **kwargs)`. Training is
            expected to modify the module. The return value typically
            summarizes the training.
        module: Any object that can be trained via *train_module* and
            for which a `CacheHandler` exists.
        args, kwargs: Forwarded to *train_module*.

    Returns:
        If the module had to be trained, this returns the return value
        of *train_module*. If the cache has been used, this returns
        `None`.

    Raises:
        `NoHandlerError`: No `CacheHandler` can handle the given
            *module*.
        `NoCacheFileError`: The cache was unavailable and the user has
            requested to `never <TrainWhen.NEVER>` train.
        `OSError`: If saving a cache file fails for I/O-related reasons.
            When loading a cache file, this exceptions is logged, but
            otherwise ignored.
        `Exception`: Any exception raised withing *train_module*.
    """
    cacher = TrainingCacher(cache=namespace.cache, train=namespace.train)
    return cacher(train_module, module, *args, **kwargs)
