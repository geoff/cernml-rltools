# SPDX-FileCopyrightText: 2020 - 2024 CERN
# SPDX-FileCopyrightText: 2023 - 2024 GSI Helmholtzzentrum für Schwerionenforschung
# SPDX-FileNotice: All rights not expressly granted are reserved.
#
# SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

"""Pytest configuration file."""

from contextlib import ExitStack
from typing import Union
from unittest.mock import MagicMock, patch

import pytest

exit_stack = ExitStack()

collect_ignore: list[str] = []


def pytest_sessionstart(session: pytest.Session) -> None:
    tensorflow = MagicMock(name="tensorflow")
    torch = MagicMock(name="torch")
    ctx = patch.dict("sys.modules")
    modules = exit_stack.enter_context(ctx)
    modules["tensorflow"] = tensorflow
    modules["torch"] = torch
    modules["torch.nn"] = torch.nn


def pytest_sessionfinish(
    session: pytest.Session, exitstatus: Union[int, pytest.ExitCode]
) -> None:
    exit_stack.close()
